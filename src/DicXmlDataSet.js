// @ts-check
const
  _ = require('lodash'),
  DicValue = require('./DicValue'),
  DicCmdUtils = require('./DicCmdUtils'),
  { DataType } = require('./XmlData'),

  xml = require('./xml');

/**
 * @param  {?Element} e
 * @return {Array<{ value: number, name: string }>}
 */
function parseEnum(e) {
  if (!e) { return []; }
  return _.map(e.getElementsByTagName('value'), function(elt) {
    return {
      value: _.toNumber(elt.getAttribute('value')),
      name: elt.getAttribute('name') || ''
    };
  });
}

/**
 * @param  {Element} data
 * @return {redim.XmlData.Data}
 */
function parseData(data) { /* eslint-disable-line complexity */
  if (_.isEmpty(data.getAttribute('type')) &&
    _.isEmpty(data.getAttribute('value'))) {
    /** @type {redim.XmlData.NestedData} */
    const ret = {
      value: _.map(xml.getChildNodes(data, 'data'), parseData),
      index: _.toNumber(data.getAttribute('index'))
    };
    if (data.hasAttribute('name')) {
      ret.name = data.getAttribute('name') || '';
    }
    return ret;
  }
  else {
    /** @type {redim.XmlData.BaseData} */
    const ret = {
      type: _.toNumber(data.getAttribute('type')),
      value: data.getAttribute('value'),
      index: _.toNumber(data.getAttribute('index'))
    };
    if (data.hasAttribute('name')) {
      ret.name = data.getAttribute('name') || '';
    }
    if (data.hasAttribute('unit')) {
      ret.unit = data.getAttribute('unit') || '';
    }
    /* $FlowIgnore: flow is ignoring the conversion inside parseValue */
    ret.value = DicXmlDataSet.parseValue(ret.value, ret.type);
    if (ret.type === DataType.ENUM) {
      const enumRet = /** @type {redim.XmlData.EnumData} */ (ret);
      enumRet.valueName = data.getAttribute('valueName') || '';
      enumRet.enum = parseEnum(data.getElementsByTagName('enum').item(0));
    }
    return ret;
  }
}

/**
 * @extends DicValue<redim.XmlData.DataSet>
 */
class DicXmlDataSet extends DicValue {
  /**
   * @param {redim.DicValue.Message<any>} rep
   */
  _setValue(rep) {
    if (rep && rep.value) {
      rep.value = DicXmlDataSet.parse(
        /** @type {redim.DicValue.Message<string>} */ (rep).value);
    }
    super._setValue(rep);
  }

  /**
   * @param  {string} xmlsource
   * @return {redim.XmlData.DataSet}
   */
  static parse(xmlsource) {
    const parser = DicCmdUtils.getXmlParser();
    var root = parser.parseFromString(xmlsource, 'text/xml').documentElement;
    if (!root) { return []; }
    var data = xml.getChildNodes(root, 'data');

    return _.map(data, parseData);
  }

  /**
   * @param {string} value
   * @param {redim.XmlData.DataType} type
   * @return {boolean|number|string}
   */
  static parseValue(value, type) { /* eslint-disable-line complexity */
    switch (type) {
    case DataType.INT64:
    case DataType.INT32:
    case DataType.INT16:
    case DataType.INT8:
    case DataType.DOUBLE:
    case DataType.FLOAT:
    case DataType.ENUM:
    case DataType.UINT32:
    case DataType.UINT64:
    case DataType.UINT8:
    case DataType.UINT16:
      return _.toNumber(value);
    case DataType.BOOL:
      return _.toNumber(value) > 0;
    default:
      return value;
    }
  }
}

DicXmlDataSet.DataType = DataType;

module.exports = DicXmlDataSet;
