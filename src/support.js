// @ts-check
const
  _ = require('lodash'),
  q = require('q'),
  debug = require('debug')('redim:support'),
  utils = require('./utils'),
  DimError = require('./DimError');

/** @enum {number} */
const RunningEnv = Object.freeze({
  NODE: 1,
  FETCH: 2,
  NO_FETCH: 3
});

function hasNotFetchSupport() {
  return typeof fetch === 'undefined' ||
    /* @ts-ignore: is called only when is initialized. look just after! */
    utils.browserInfo.satisfies({ firefox: '<65' });
}

// It will store ws / fetch implementations based on the environment
/** @type {RunningEnv} */
var currentEnv;

if (_.isNil(utils.browserInfo)) {
  currentEnv = RunningEnv.NODE;
}
else if (hasNotFetchSupport()) {
  currentEnv = RunningEnv.NO_FETCH;
}
else {
  currentEnv = RunningEnv.FETCH;
}

/**
 * @brief Streaming mode implemented with Fetch
 * @param {string} url
 * @returns {Promise<redim.fetchCtrl>}
 */
function fetchSupported(url) {
  // @ts-ignore
  return q()
  // @ts-ignore
  .then(fetch.bind(null, url, {
    cache: 'no-store',
    headers: { 'Cache-Control': 'no-cache' }
  }))
  .then((res) => {
    if (!res.body) {
      debug('fetch: body missing');
      return q.reject(new DimError.NotSupported('no body in fetch'));
    }
    if (!res.ok) {
      debug('fetch: response not ok');
      return q.reject(
        new DimError.NetworkError('error on fetch: response not ok'));
    }
    const reader = res.body.getReader();
    var def = q.defer();
    /** @type {redim.fetchCtrl} */
    var ctrl = {
      abort: reader.cancel.bind(reader, 'aborted'),
      resume: () => { reader.read().then(_read); return def.promise; },
      onmessage: null
    };

    /** @type {(ret: any) => Promise<any> | void} */
    var _read = (ret) => {
      if (!ret || ret.done || !ret.value) {
        return def.resolve();
      }
      // @ts-ignore
      if (_.attempt(ctrl.onmessage || _.noop, ret.value) === true) {
        return ctrl.abort();
      }
      return reader.read().then(_read);
    };
    return ctrl;
  });
}

/**
 * @brief Streaming implementation over XMLHttpRequest
 * @param {string} url
 * @returns {Promise<redim.fetchCtrl>}
 */
function fetchOverXHR(url) {
  // @ts-ignore
  return q()
  .then(() => {
    var xhr = new XMLHttpRequest();
    var seenBytes = 0;
    var enc = new TextEncoder();
    var def = q.defer();
    /** @type {redim.fetchCtrl} */
    var ctrl = {
      abort: () => xhr.abort(),
      resume: () => {
        xhr.open('GET', url);
        seenBytes = 0;
        xhr.send();
        return def.promise;
      },
      onmessage: null
    };

    xhr.onloadend = function() {
      if ((xhr.status > 0 && xhr.status < 100) || xhr.status >= 400) {
        utils.rejectIfPending(def,
          new DimError.NetworkError('Network request failed'));
      }
      else {
        utils.resolveIfPending(def, undefined);
      }
    };

    // eslint-disable-next-line consistent-return
    xhr.onreadystatechange = function() {
      if (xhr.readyState === XMLHttpRequest.LOADING) {
        var newData = xhr.response.substr(seenBytes);

        var newDataByteArray = enc.encode(newData);
        seenBytes = xhr.responseText.length;
        // @ts-ignore
        if (_.attempt(ctrl.onmessage || _.noop, newDataByteArray) === true) {
          return ctrl.abort();
        }
      }
    };
    return ctrl;
  });
}

/**
 * @return {{ WebSocket: any, fetch: (url: string) => Promise<redim.fetchCtrl> }}
 */
function resolveMod() {
  switch (currentEnv) {
  case RunningEnv.NODE:
    return require('./support-node'); /* eslint-disable-line global-require */
  case RunningEnv.FETCH:
    return {
      WebSocket: (typeof WebSocket !== 'undefined') ? WebSocket : undefined,
      fetch: fetchSupported
    };
  default:
  case RunningEnv.NO_FETCH:
    return {
      WebSocket: (typeof WebSocket !== 'undefined') ? WebSocket : undefined,
      fetch: fetchOverXHR
    };
  }
}

module.exports = resolveMod();
