import { expectType } from 'tsd';
import {
  DicCmd, DicValue, DicXmlCmd, DicXmlDataSet,
  DicXmlParams, DicXmlRpc, DicXmlState, DicXmlValue, DimError,
  DnsClient } from '.';

/* DIM */
(async function() {
  var dns: DnsClient = DnsClient.wrap('localhost');

  dns = DnsClient.wrap(dns);
  dns.unref();

  const node: redim.NodeInfo = await dns.query('/test');
  expectType<string>(node.node);

  dns.unref();

  expectType<number|undefined>(
    await DicValue.get<number>("/test", null, "localhost"));
  const value = new DicValue<string>('/test',
    { watch: true, initial: true, timer: 1000 }, dns);

  expectType<string | undefined>(
    await value.promise());
  expectType<string | undefined>(value.value);

  await DicCmd.invoke('/test', [ 1, 2 ], null, dns);
}());

/* DIM-XML */
(async function() {
  const state = new DicXmlState('/test', null, 'localhost');
  await state.promise();
}());

/* DIC */
(async function() {
  var dnsClient: DnsClient = new DnsClient('dnsserver.cern.ch');

  expectType<number>(
    await DicXmlCmd.invoke<number, number>('/test', 42, null,
      'dnsserver.cern.ch:2505'));
  expectType<any>(
    await DicXmlCmd.invoke('/test', 42, null,
      'dnsserver.cern.ch:2505'));

  const cmd = new DicXmlCmd<{ test: number}, string>('/test', null, dnsClient);
  expectType<string>(await cmd.invoke({ test: 42 }));

  const cmd2 = new DicXmlCmd('/test', null, dnsClient);
  expectType<any>(await cmd2.invoke({ test: 42 }));

  var dataset: DicXmlDataSet = new DicXmlDataSet('/test', null, dnsClient);
  expectType<redim.XmlData.DataSet|undefined>(await dataset.promise());
  expectType<redim.XmlData.DataSet>(DicXmlDataSet.parse('<xml>'));

  var params: DicXmlParams = new DicXmlParams('/test', null, dnsClient);
  expectType<redim.XmlData.DataSet|undefined>(await params.promise());
  expectType<any>(
    await params.setParams([ { value: 12, index: 3 } ]));

  var rpc = new DicXmlRpc<string, number>('/test', null, 'localhost');
  expectType<number>(await rpc.invoke('test'));

  var state = new DicXmlState('/state', { stamped: true }, dnsClient);
  expectType<redim.XmlState.State|undefined>(await state.promise());
  expectType<redim.XmlState.State|undefined>(
    await DicXmlState.get('/test', null, 'proxy-1'));

  var value: DicXmlValue = new DicXmlValue('/test', null, dnsClient);
  expectType<Element|undefined>(await value.promise());
  expectType<Element|undefined>(await DicXmlValue.get('/test', null,
    dnsClient));

  dnsClient.unref();
}());

/* ERRORS */
expectType<string>(DimError.name);
expectType<string>(DimError.NotFound.name);
expectType<string>(DimError.InvalidParam.name);
expectType<string>(DimError.NotSupported.name);
expectType<string>(DimError.NetworkError.name);

const err = new DimError.NotFound("not found");
expectType<string>(err.name);
