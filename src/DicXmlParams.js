// @ts-check
const
  _ = require('lodash'),

  DicXmlCmd = require('./DicXmlCmd'),
  DicXmlDataSet = require('./DicXmlDataSet');

/**
 * @typedef {import('./DnsClient')} DnsClient
 */

/**
 * @param  {redim.XmlData.ParamPart} param
 * @return {redim.XmlJsObject}
 */
function paramPartToXml(param) {
  param = _.clone(param);
  if (_.get(param, 'type') === DicXmlDataSet.DataType.BOOL) {
    param.value = param.value ? 1 : 0;
  }
  if (_.isArray(param.value)) {
    // Nested Data
    const keepValue = _.clone(param.value);

    // @ts-ignore: transforming param
    delete param.value;
    /** @type {Array<redim.XmlJsObject>} */
    const data = [];
    _.forEach(keepValue, (value) => {
      const par = paramPartToXml(value);
      data.push(par);
    });
    return { '$': param, data: data };
  }
  return { '$': param };
}

class DicXmlParams extends DicXmlDataSet {
  /**
   * @param {string} service
   * @param {?redim.DicValue.Options} options
   * @param {string|DnsClient|redim.NodeInfo} dns
   */
  constructor(service, options, dns) {
    super(service + '/Aqn', options, dns);
    this.cmd = new DicXmlCmd(service,
      _.pick(options, [ 'timeout', 'proxy' ]), dns);
  }

  close() {
    this.cmd.close();
    super.close();
  }

  /**
   * @param {Array<redim.XmlData.ParamPart>|redim.XmlData.ParamPart} params
   * @param {?number=} timeout
   * @return {Q.Promise<any>}
   */
  setParams(params, timeout = DicXmlCmd.TIMEOUT) {
    var prep = _.isArray(params) ?
      _.map(params, paramPartToXml) : paramPartToXml(params);
    return this.cmd.invoke({ parameters: { data: prep } }, null, timeout);
  }
}

module.exports = DicXmlParams;
