// @ts-check
const
  _ = require('lodash'),
  debug = require('debug')('redim:client:xml'),

  DicValue = require('./DicValue'),
  DicCmdUtils = require('./DicCmdUtils');

/**
 * @typedef {import('./DnsClient')} DnsClient
 */

/**
 * @template T=Element
 * @extends DicValue<T>
 */
class DicXmlValue extends DicValue {
  /**
   * @param {redim.DicValue.Message<any>} rep
   */
  _setValue(rep) {
    if (rep && _.isString(rep.value)) {
      rep.value = DicXmlValue.parse(rep.value);
    }
    super._setValue(rep);
  }

  /**
   * @param  {string} xmlsource
   * @return {Element|undefined}
   */
  static parse(xmlsource) {
    const parser = DicCmdUtils.getXmlParser();
    var root = parser.parseFromString(xmlsource, 'text/xml').documentElement;
    if (!root) {
      debug('failed to parse:', xmlsource);
    }
    return root;
  }

  /**
   * @template T=Element
   * @param {string} service
   * @param {?redim.DicValue.Options} options
   * @param {string|DnsClient|redim.NodeInfo} dns
   * @return {Q.Promise<T|undefined>}
   */
  static get(service, options, dns) {
    // @ts-ignore
    return DicValue.get(service, options, dns)
    .then((ret) => DicXmlValue.parse(ret || ''));
  }
}

module.exports = DicXmlValue;
