
import * as wsWebSocket from 'ws';

export as namespace redim;

export interface WebSocket extends wsWebSocket {
  _ref: number;
  sid?: number;
  req?: { [sid: number]: (msg: any) => any }
}
export type MessageEvent = wsWebSocket.MessageEvent;

export interface NodeNetInfo {
  node: string
  port: number
}

export interface NodeInfo extends NodeNetInfo {
  node: string;
  task: string;
  address: (number|string|null);
  pid: number;
  port: number;
  protocol: number;
  format: number|null;
  definition: string;
  sid: number;
}

export interface NullNodeInfo { node?: null }

export namespace DicValue {
  export interface Params {
    name: string;
    watch: boolean;
    initial: boolean;
    timer: number;
    stamped: boolean;
    timeout?: number;
    definition?: string;
    sid?: number;
    proxy?: string;
  }

  export type Options = Partial<Params>;

  export interface Message<T=any> {
    name: string;
    sid: string|number;
    value: T|undefined;
    timestamp?: number;
    quality?: number;
  }
}

export namespace DicCmd {
  export interface Request<T=any> {
    name: string;
    sid?: string|number;
    data: T;
    definition?: string;
  }
  export type Reply<T=any> = DicValue.Message<T>
}

export namespace DicXmlCmd {
  export interface Params {
    key: number;
    timeout: number;
    proxy: string;
  }

  export type Options = Partial<Params>;

  export interface Reply {
    key?: number;
    status: number;
    error: number;
    message?: string|null;
    xml?: any;
  }
}

export namespace XmlState {
  export interface State extends Value {
    errors: Array<XmlState.Error>;
    warnings: Array<XmlState.Warning>;
    description: Array<XmlState.Value>;
  }

  export interface Error {
    code: number;
    message: string;
  }
  export type Warning = Error
  export interface Value {
    value: number;
    strValue: string;
  }
}

export namespace XmlData {
  export interface NestedData {
    name?: string;
    index: number;
    type?: DataType.NESTED;
    value: Array<Data<any>>
  }
  export interface BaseData<T=any> {
    name?: string;
    index: number;
    unit?: string;
    type: DataType;
    value: T;
  }
  export interface EnumData {
    name?: string;
    index: number;
    unit?: string;
    type: DataType.ENUM;
    value: number;
    valueName: string;
    enum: EnumDesc;
  }
  export type Data<T=any> = BaseData<T> | NestedData | EnumData

  export enum DataType {
    INVALID = -1,
    NESTED = 0,
    INT32 = 1,
    DOUBLE = 2,
    STRING = 3,
    INT64 = 4,
    ENUM = 5,
    INT8 = 6,
    INT16 = 7,
    FLOAT = 8,
    BOOL = 9,
    UINT32 = 11,
    UINT64 = 14,
    UINT8 = 16,
    UINT16 = 17
  }

  export type EnumDesc = Array<{ name: string, value: number }>
  export type DataSet = Array<Data>

  export interface ParamPart {
    type?: DataType;
    index: number;
    value: string|boolean|number|Array<ParamPart>
  }
}

export interface XmlJsAttrList { [attrName:string]: any }

export interface XmlJsObject {
  '$'?: XmlJsAttrList
  '$textContent'?: string
  [tagName:string]: Array<XmlJsObject> | XmlJsObject | any | string
}

export interface fetchCtrl {
  abort: () => void;
  onmessage: ((data: any) => any) | null;
  resume: () => Q.Promise<void>;
}

