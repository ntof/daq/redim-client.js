// @ts-check

const
  _ = require('lodash'),
  debug = require('debug')('redim:client:dns'),
  DicValue = require('./DicValue'),
  DicCmd = require('./DicCmd');

/**
 * @typedef {import('./DnsClient')} DnsClient
 * @typedef {import('./types').NodeInfo} NodeInfo
 */

/**
 * @brief retrieve service info on remote service
 * @param  {string} name service name
 * @param {?redim.DicValue.Options} options
 * @param {string|DnsClient|redim.NodeInfo} dns
 * @param {?number=} timeout
 * @return {Q.Promise<{ [serviceName: string]: string }>}
 */
function serviceInfo(name, options, dns, timeout = DicCmd.TIMEOUT) {
  const opts = _.assign(options, {
    sid: 1,
    definition: 'C,C|RPC',
    timeout });
  return DicCmd.invoke('DIS_DNS/SERVICE_INFO/RpcIn', name, opts, dns)
  .then((ret) => _.transform(_.split(ret.value, '\n'), function(ret, srvDef) {
    var srv = _.toString(srvDef);
    var idx = srv.indexOf('|');
    var def = (idx !== -1) ? srv.slice(idx + 1) : null;
    if (_.isNil(def)) {
      debug('failed to parse definition:', srv);
      return;
    }

    // @ts-ignore
    ret[srv.slice(0, idx)] = def;
  }, {}));
}

/**
 * DIM Client Value for retrieving the DIM Service Provider list
 * @extends DicValue<Array<{ task: string, node: string, pid: number, removed?: boolean }>>
 */
// @ts-ignore: overridden 'get' property does not require service parameter
class DicServerList extends DicValue {
  /**
  * create a specific DicValue object for DIS_DNS/SERVER_LIST service
  * @param {?redim.DicValue.Options} options
  * @param {string|DnsClient|redim.NodeInfo} dns
  *
  * @details available options:
  * - timeout: request timeout value (default: DisClient.TIMEOUT)
  * - stamped: make a stamped request (default: false)
  */
  constructor(dns, options) {
    super('DIS_DNS/SERVER_LIST', options, dns);
    /** @type {{ [taskName: string]: { task: string, node: string, pid: number } }} */
    this.db = {};
  }

  /**
   * Overridden method for service monitoring purpose
   * @param {any} rep
   */
  _setValue(rep) {
    if (_.has(rep, 'data')) {
      rep.value = update(_.toString(rep.data), this.db);
    }
    super._setValue(rep);
  }

  /**
   * @brief retrieve the server list once from a DNS Server
   * @param {?redim.DicValue.Options} options
   * @param {string|DnsClient|redim.NodeInfo} dns
   * @param {?number=} timeout
   * @return {Q.Promise<Array<{task: string, node: string, pid: number,
   *  removed?: boolean }>|void>}
   */
  static get(options, dns, timeout = DicValue.TIMEOUT) {
    const opts = _.assign(options, { timeout });
    return DicValue.get('DIS_DNS/SERVER_LIST', opts, dns)
    .then((ret) => {
      return _.isBuffer(ret) ? update(_.toString(ret), {}) : undefined;
    });
  }
}

/**
 * @brief Parse the list of server
 * @param {string} data
 * @param {{ [server: string]: { task: string, node: string, pid: number } }} db
 * @return { Array<{ task: string, node: string, pid: number }> }
 * @details db is modified by this function
 */
function update(data, db) {
  const info = _.split(data, '\0');
  const servers = _.isEmpty(info[0]) ? null : _.split(info[0], '|');
  const pids = _.isEmpty(info[1]) ? null : _.split(info[1], '|');
  if (servers && pids) {
    let i = 0;
    _.forEach(servers, (server) => {
      const info = _.split(server, '@');
      const prefix = _.first(info[0]);
      const taskName = ((prefix === '-') || (prefix === '+')) ?
        info[0].slice(1) : info[0];

      if (prefix === '-') {
        delete db[taskName];
        i++;
      }
      else {
        /** @type {{ task: string, node: string, pid: number,
         * removed?: boolean }} */
        const srv = db[taskName] || {};
        db[taskName] = srv;
        srv.task = taskName;
        srv.node = info[1];
        srv.pid =  _.toNumber(pids[i++]);
      }
    });
  }
  return _.values(db);
}

module.exports = {
  serviceInfo,
  serverList: DicServerList.get,
  DicServerList
};
