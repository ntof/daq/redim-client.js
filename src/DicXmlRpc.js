// @ts-check
const
  _ = require('lodash'),

  { EventEmitter } = require('events'),
  DimError = require('./DimError'),
  DicXmlCmd = require('./DicXmlCmd'),
  DicCmdUtils = require('./DicCmdUtils'),
  DicCmd = require('./DicCmd'),
  xml = require('./xml'),
  { getNextKey } = require('./utils');

/**
 * @typedef {import('./DnsClient')} DnsClient
 */

/**
 * @template Args=any
 * @template Return=any
 */
class DicXmlRpc extends EventEmitter {
  /**
   * @param {string} service
   * @param {?redim.DicXmlCmd.Options} options
   * @param {string|DnsClient|redim.NodeInfo} dns
   */
  constructor(service, options, dns) {
    super();
    this.service = service + '/RpcIn';
    /** @type {?redim.DicXmlCmd.Options} */
    this.options = options;
    this.dns = dns;
  }

  close() {
    this.emit('close');
  }

  /**
   * @param  {Args} args
   * @param  {?number=} key
   * @param {?number=} timeout
   * @return {Q.Promise<Return>}
   */
  invoke(args, key, timeout) {
    var timeoutVal = _.defaultTo(timeout, DicXmlCmd.TIMEOUT);
    const proxy = _.get(this.options, 'proxy');
    var strKey = _.toString(key || getNextKey());

    return DicCmd.dnsResolve(this.service, this.dns)
    .then((dns) => {
      this.dns = dns;
      var root = DicCmdUtils.generateXmlCommand(strKey, args);
      var params = _.assign({ definition: 'C,C|RPC', sid: 1  },
        { name: this.service, data: xml.serializeToString(root) });

      return DicCmd.rawInvoke(DicCmd.getUrl(dns, proxy), params, timeoutVal)
      .then((rep) => DicCmdUtils.filterXmlReply(rep.value, strKey))
      .then((rep) => DicCmdUtils.checkXmlReply(rep))
      .timeout(timeoutVal * 1000, 'timeout');
    })
    .catch((err) => {
      if (err === 'timeout') {
        throw new DimError.Timeout('timeout on: ' + this.service);
      }
      throw err;
    });
  }

  /**
   * @template Args=any
   * @template Return=any
   * @param {string} service
   * @param {Args} args
   * @param {?redim.DicXmlCmd.Options} options
   * @param {DnsClient|redim.NodeInfo|string} dns
   * @return {Q.Promise<Return>}
   */
  static invoke(service, args, options, dns) {
    var cmd = new DicXmlRpc(service, options, dns);
    return cmd.invoke(args, _.get(options, 'key'))
    .finally(cmd.close.bind(cmd));
  }
}

module.exports = DicXmlRpc;
