// @ts-check
const
  debug = require('debug')('redim:client:cmd'),
  _ = require('lodash'),
  q = require('q'),
  axios = require('axios').default,

  DnsRegistry = require('./DnsRegistry'),
  DimError = require('./DimError');

/**
 * @typedef {import('./DnsClient')} DnsClient
 * @typedef {import('./types').NodeNetInfo} NodeNetInfo
 * @typedef {import('./types').NodeInfo} NodeInfo
 */

/**
 * @template T=any
 * @param  {?string} url
 * @param  {redim.DicCmd.Request<T>} params
 * @param  {number} timeout [description]
 * @return {Q.Promise<any>}
 */
function rawInvoke(url, params, timeout) {
  if (!url) {
    return q.reject(new DimError.NotFound('service unavailable: ' +
      params.name));
  }
  debug('sending command', url, params);
  return q()
  .then(() => axios.post(url, params, { timeout: timeout * 1000 }))
  .then((rep) => rep.data)
  .catch((err) => {
    if (_.startsWith(_.get(err, 'message'), 'timeout')) {
      throw new DimError.Timeout('timeout on: ' + params.name);
    }
    throw err;
  });
}

/**
 * @param  {{ node: string, port: number }} info
 * @param {?string=} proxy
 * @return {?string}
 */
function getUrl(info, proxy) {
  if (!info || !info.node || !_.isNumber(info.port)) {
    return null;
  }
  if (!proxy) {
    proxy = 'http://';
  }
  else if (!_.endsWith(proxy, '/')) {
    proxy += '/';
  }
  return `${proxy}${info.node}:${info.port}/dis/command`;
}

/**
 * @param {string} service
 * @param {string|DnsClient|NodeNetInfo} dns
 * @return {Q.Promise<NodeInfo>}
 */
function dnsResolve(service, dns) {
  var def = q.defer();
  // Case: Already a Node Info
  if (_.has(dns, 'node') && _.has(dns, 'port')) {
    def.resolve(dns);
  }
  else {
    // Case: Get it from DNS
    // @ts-ignore: checked above
    var dnsCli = DnsRegistry.get(dns);
    if (!dnsCli) {
      def.reject(new DimError.InvalidParam('dns information missing'));
    }
    else {
      dnsCli.query(service)
      .then((info) => {
        if (!info || !info.node) {
          def.reject(new DimError.NotFound('service unavailable: ' + service));
        }
        def.resolve(info);
      })
      .catch((err) => def.reject(err))
      /* @ts-ignore: checked above */
      .finally(() => dnsCli.unref());
    }
  }
  return def.promise;
}

/**
 * @template T=void
 * @param  {string} service
 * @param  {any} args
 * @param  {?Partial<{
 *  timeout: number,
 *  proxy: string,
 *  definition: string,
 *  sid: number}>} opts
 * @param  {string|DnsClient|NodeInfo} dns
 * @return {Q.Promise<T>}
 */
function invoke(service, args, opts, dns) {
  const timeout = _.get(opts, 'timeout', DicCmd.TIMEOUT);
  const proxy = _.get(opts, 'proxy');
  var params = _.assign(_.pick(opts, [ 'definition', 'sid' ]),
    { name: service, data: args });

  return dnsResolve(service, dns)
  .then((dns) => {
    if (!_.has(params, 'definition')) {
      params.definition = dns.definition;
    }
    return rawInvoke(getUrl(dns, proxy), params, timeout);
  });
}

const DicCmd = {
  TIMEOUT: 5, /* in seconds like other DIM timeouts */
  dnsResolve: dnsResolve,
  invoke: invoke,
  rawInvoke: rawInvoke,
  getUrl: getUrl
};

module.exports = DicCmd;
