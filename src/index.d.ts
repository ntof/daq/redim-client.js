// / <reference types="node" />
// / <reference types="xmldom" />
/* eslint-disable max-len,max-lines */
import { EventEmitter } from 'events';

export = redim;
export as namespace redim;

declare namespace redim {

  class DimError extends Error {
    constructor(message?: string);
  }

  namespace DimError {
    class NotFound extends DimError {
      constructor(message?: string);
    }
    namespace NotFound {
      const name: string;
    }
    class InvalidParam extends DimError {
      constructor(message?: string);
    }
    namespace InvalidParam {
      const name: string;
    }
    class NotSupported extends DimError {
      constructor(message?: string);
    }
    namespace NotSupported {
      const name: string;
    }
    class NetworkError extends DimError {
      constructor(message?: string);
    }
    namespace NetworkError {
      const name: string;
    }
    const name: string;
  }

  interface NodeNetInfo {
    node: string
    port: number
  }

  interface NodeInfo extends NodeNetInfo {
    node: string;
    task: string;
    address: (number|string|null);
    pid: number;
    port: number;
    protocol: number;
    format: number|null;
    definition: string;
    sid: number;
  }

  class DnsClient extends EventEmitter {
    static wrap(dns: DnsClient | string): DnsClient;

    constructor(url: string);

    url: string;

    closed: boolean;

    ref(): void;

    unref(): void;

    close(): void;

    query(service: string): Promise<NodeInfo>;

    refreshQuery(service: string): void;
  }

  namespace xml {
    function createDocument(tag: string): Document;
    function serializeToString(node: Node): string;
    function fromJs(node: Element, args: XmlJsObject): Element;
    function toJs(node: Element): XmlJsObject;
    function getText(node: Element): string;
    function getChildNodes(node: Node | Element, tag: string): Array<Element>;
  }

  namespace DicCmdUtils {
    function getXmlParser(): DOMParser;
    function generateXmlCommand(strKey: string, args: any): Element;
    function filterXmlReply(value: string, strKey: string): DicXmlCmd.Reply | undefined;
    function checkXmlReply(reply?: (DicXmlCmd.Reply | null) | undefined): DicXmlCmd.Reply | null;
    function createError(error: number, message: string): DicXmlCmd.Reply;

    enum Status {
      OK = 2, ERROR = 3
    }
  }

  class DicValue<T> extends EventEmitter {
    static get<T>(service: string, options: DicValue.Options | null, dns: string | DnsClient | NodeInfo): Promise<T|undefined>;

    constructor(service: string, options: DicValue.Options | null, dns: string | DnsClient | NodeInfo);

    service: string;

    value: T | undefined;

    closed: boolean;

    timestamp: number | null;

    quality: number | null;

    close(): void;

    isConnected(): boolean;

    promise(): Promise<T | undefined>;

    then<K>(fulfill: (value: T | undefined) => K | Promise<K> | Promise<K>, reject: (error: Error | string) => any): Promise<K>;
  }
  namespace DicValue {
    const TIMEOUT: number;
    export interface Params {
      name: string;
      watch: boolean;
      initial: boolean;
      timer: number;
      stamped: boolean;
      definition?: string;
      sid?: number;
      proxy?: string;
    }

    export type Options = Partial<Params>;

    export interface Message<T=any> {
      name: string;
      sid: string|number;
      value: T|undefined;
      timestamp?: number;
      quality?: number;
    }
  }

  export namespace DicDis {
    export function serviceList (taskName: string, options: Partial<DicValue.Params>, dns: (DnsClient| NodeInfo | string) | null, timeout?: (number | null) | undefined): Promise<void | {
        [serviceName: string]: string;
    }>;

    export class DicServicesList<T={ [serviceName: string]: string }> extends DicValue<T> {
      constructor(taskName: string, options: Partial<DicValue.Params>, dns?: (DnsClient| NodeInfo | string) | null);
      db: { [taskName: string]: string };
    }
  }

  export namespace DicDns {
    export function serviceInfo(name: string, options: Partial<DicValue.Params>, dns: (DnsClient| NodeInfo | string) | null, timeout?: (number | null) | undefined): Promise<{ [serviceName: string]: string }>;

    export function serverList(options: Partial<DicValue.Params>, dns: (DnsClient| NodeInfo | string) | null, timeout?: (number | null) | undefined): Promise<void |
    {
      task: string;
      node: string;
      pid: number;
      removed?: boolean;
    }[]>;

    export class DicServerList<T=Array<{task: string, node: string, pid: number, removed?: boolean }>> extends DicValue<T> {
      constructor(dns?: ((DnsClient| NodeInfo | string) | null) | undefined, options?: Partial<DicValue.Params>);
    }
  }

  class DicXmlCmd<Args=any, Return=any> extends EventEmitter {
    static invoke<Args=any, Return=any>(service: string, args: Args, options: DicXmlCmd.Options | null, dns: DnsClient | NodeInfo | string): Promise<Return>;

    constructor(service: string, options: DicXmlCmd.Options | null, dns: string | DnsClient | NodeInfo);

    service: string;

    closed: boolean;

    value: DicValue<string>;

    close(): void;

    invoke(args: Args, key?: (number | null) | undefined, timeout?: (number | null) | undefined): Promise<Return>;
  }
  namespace DicXmlCmd {
    const TIMEOUT: number;
    export interface Params {
      key: number;
      timeout: number;
      proxy: string;
    }

    export type Options = Partial<Params>;

    export interface Reply {
      key?: number;
      status: number;
      error: number;
      message?: string|null;
      xml?: any;
    }
  }

  namespace XmlData {
    export interface NestedData {
      name?: string;
      index: number;
      type?: DataType.NESTED;
      value: Array<Data<any>>
    }
    export interface BaseData<T=any> {
      name?: string;
      index: number;
      unit?: string;
      type: DataType;
      value: T;
    }
    export interface EnumData {
      name?: string;
      index: number;
      unit?: string;
      type: DataType.ENUM;
      value: number;
      valueName: string;
      enum: EnumDesc;
    }
    export type Data<T=any> = BaseData<T> | NestedData | EnumData

    enum DataType {
      INVALID = -1,
      NESTED = 0,
      INT32 = 1,
      DOUBLE = 2,
      STRING = 3,
      INT64 = 4,
      ENUM = 5,
      INT8 = 6,
      INT16 = 7,
      FLOAT = 8,
      BOOL = 9,
      UINT32 = 11,
      UINT64 = 14,
      UINT8 = 16,
      UINT16 = 17
    }

    export type EnumDesc = Array<{ name: string, value: number }>
    export type DataSet = Array<Data>

    export interface ParamPart {
      type?: DataType;
      index: number;
      value: string|boolean|number|Array<ParamPart>
    }
  }

  class DicXmlDataSet extends DicValue<XmlData.DataSet> {
    static parse(xmlsource: string): XmlData.DataSet;

    static parseValue(value: string, type: XmlData.DataType): boolean | number | string;

    constructor(service: string, options: Partial<DicValue.Params> | null, dns: string | NodeInfo | DnsClient);
  }
  namespace DicXmlDataSet {
    const DataType: typeof XmlData.DataType;
  }

  class DicXmlParams extends DicXmlDataSet {
    constructor(service: string, options: DicValue.Options | null, dns: string | DnsClient | NodeInfo);

    cmd: DicXmlCmd<any, any>;

    setParams(params: Array<XmlData.ParamPart> | XmlData.ParamPart): Promise<any>;
  }

  class DicXmlRpc<Args, Return> extends EventEmitter {
    static invoke<Args, Return>(service: string, args: Args, options: DicXmlCmd.Options | null, dns: DnsClient | NodeInfo | string): Promise<Return>;

    constructor(service: string, options: DicXmlCmd.Options | null, dns: string | DnsClient | NodeInfo);

    service: string;

    options: DicXmlCmd.Options | null;

    dns: string | NodeInfo | DnsClient;

    close(): void;

    invoke(args: Args, key?: number | null | undefined, timeout?: number | null | undefined): Promise<Return>;
  }

  class DicXmlValue<T=Element> extends DicValue<T> {
    static parse<T=Element>(xmlsource: string): T | null;

    static get<T=Element>(service: string, options: DicValue.Options | null, dns: string | DnsClient | NodeInfo): Promise<T | undefined>;

    constructor(service: string, options: Partial<DicValue.Params> | null, dns: string | NodeInfo | DnsClient);
  }

  class DicXmlState extends DicXmlValue<XmlState.State> {
    static parse<T=XmlState.State>(xmlsource: string): T;

    static get<T=XmlState.State>(service: string, options: DicValue.Options | null, dns: string | DnsClient | NodeInfo): Promise<T>;

    constructor(service: string, options: Partial<DicValue.Params> | null, dns: string | NodeInfo | DnsClient);
  }

  namespace XmlState {
    export interface State extends Value {
      errors: Array<XmlState.Error>;
      warnings: Array<XmlState.Warning>;
      description: Array<XmlState.Value>;
    }

    export interface Error {
      code: number;
      message: string;
    }
    export type Warning = Error
    export interface Value {
      value: number;
      strValue: string;
    }
  }

  namespace DicCmd {
    const TIMEOUT: number;
    function dnsResolve(service: string, dns: string | NodeNetInfo | DnsClient): Promise<NodeInfo>;
    function invoke<T>(service: string, args: any, opts: Partial<{
            timeout: number;
            proxy: string;
            definition: string;
            sid: number;
        }> | null, dns: string | NodeInfo | DnsClient): Promise<T>;

    function getUrl(info: {
            node: string;
            port: number;
        }, proxy?: string | null | undefined): string | null;

    export interface Request<T=any> {
      name: string;
      sid?: string|number;
      data: T;
      definition?: string;
    }
    export type Reply<T=any> = DicValue.Message<T>
  }

  export interface XmlJsAttrList { [attrName:string]: any }

  export interface XmlJsObject {
    '$'?: XmlJsAttrList
    '$textContent'?: string
    [tagName:string]: Array<XmlJsObject> | XmlJsObject | any | string
  }
}
