// @ts-check
const
  _ = require('lodash'),
  axios = require('axios').default,
  debug = require('debug')('redim:client:dns'),
  q = require('q'),
  { EventEmitter } = require('events'),

  PreJSON = require('./PreJSON'),
  utils = require('./utils'),
  support = require('./support'),
  { NotSupported, NetworkError } = require('./DimError'),
  ConnManager = require('./ConnManager');

/**
 * @typedef {string} ServiceName
 * @typedef {number} Sid
 */

/**
 * @brief DIM Dns Client
 * @details emits:
 *  - service:<name> when service is updated
 *  - close when DnsClient is closed
 */
class DnsClient extends EventEmitter {
  /**
   * @param {string} url
   */
  constructor(url) {
    super();
    this.url = url;
    this._ref = 1;
    /** @type {{ [serviceName: string]: {
      sid: Sid,
      node?: redim.NodeInfo,
      reqs: Array<Q.Deferred<redim.NodeInfo|redim.NullNodeInfo>> }
    }} */
    this._srvMap = {};
    /** @type {{ [sid: number]: ServiceName }} */
    this._sidMap = {};
    this._nextSid = 1;
    this.closed = false;
    /** @type {?Q.Promise<redim.WebSocket>} */
    this._ws = null;

    this._connect();
  }

  _connect() {
    if (support.WebSocket) {
      var url = utils.toWs(this.url);

      this._ws = ConnManager.webSocket(url, true)
      // $FlowIgnore is a q promise
      .tap((ws) => {
        ws.onerror = () => {
          this._ws = utils.reject('socket error');
          this._close(new NetworkError('connection error'));
        };
        ws.onclose = () => {
          this._ws = utils.reject('remote close');
          this._close(new NetworkError('disconnected'));
        };
        ws.onmessage = this._onMessage.bind(this);
      });
      this._ws.catch(_.noop);
    }
    else {
      this._ws = utils.reject('unsupported');
    }
  }

  ref() {
    if (this._ref++ <= 0) {
      debug('refcounting a dead object');
    }
  }

  unref() {
    if (--this._ref === 0) {
      debug('closing dns', this.url);
      this.close();
    }
  }

  close() {
    this._close();
  }

  /**
   * @param {?Error=} error
   */
  _close(error) {
    if (!this.closed) {
      this.closed = true;
      this.emit('close', error);
      // @ts-ignore
      this._ws.then((ws) => {
        ws.close(1000, 'closed');
        this._ws = utils.reject('closed');
      }, _.noop)
      .finally(() => {
        _.forEach(this._sidMap, (name) => {
          _.unset(this._srvMap, [ name, 'node' ]);
          this._clearPending(name, error, null);
          this.emit('service:' + name, { node: null });
        });
        this.removeAllListeners();
      });
    }
  }

  /**
   * @param {string} service
   * @return {Q.Promise<redim.NodeInfo>}
   */
  query(service) {
    // @ts-ignore
    return this._ws
    .then(
      (ws) => this._wsQuery(service, ws),
      () => {
        return this._fetchQuery(service)
        .catch((err) => {
          if (_.get(err, 'name') === NotSupported.name) {
            return q()
            .then(() => axios.get(this.url,
              { params: { name: service },
                transformResponse: [ utils.parseReply ] }))
            .then((ret) => ret.data);
          }
          throw err;
        })
        .catch((err) => {
          throw utils.wrapError(NetworkError, err);
        });
      }
    );
  }

  /**
   * @param {string} service
   * @return {Q.Promise<void>}
   */
  refreshQuery(service) {
    const srv = this._srvMap[service];
    if (!srv || !this._ws) { return q.reject('no such service'); }

    return this._ws
    .then(
      (ws) => ws.send(JSON.stringify({ sid: srv.sid, name: service })),
      (e) => {
        debug('Can\'t refresh fetch dns requests !', e);
        throw utils.wrapError(NotSupported, 'refresh operation not supported');
      });
  }

  /**
   * @param {string} service
   * @param {redim.WebSocket} ws
   * @return {redim.NodeInfo|Q.Promise<redim.NodeInfo>}
   */
  _wsQuery(service, ws) {
    var srv = this._srvMap[service];
    if (srv && srv.node) {
      return srv.node;
    }
    if (!srv) {
      const sid = this._nextSid++;
      this._srvMap[service] = { sid, reqs: [] };
      this._sidMap[sid] = service;
      ws.send(JSON.stringify({ sid, name: service }));
    }
    var def = q.defer();
    this._srvMap[service].reqs.push(def);
    return def.promise;
  }

  /**
   * @param {string} service
   * @return {Q.Promise<redim.NodeInfo>}
   */
  _fetchQuery(service) {
    var srv = this._srvMap[service];
    if (srv) {
      var def = q.defer();
      srv.reqs.push(def);
      return def.promise;
    }
    const sid = this._nextSid++;
    this._sidMap[sid] = service;
    this._srvMap[service] = { sid, reqs: [] };
    return q()
    .then(() => {
      /** @type {Q.Deferred<redim.NodeInfo>} */
      var def = q.defer();

      return support.fetch(this.url + '?' + utils.toQueryString(
        { name: service, sid }))
      .then((ctrl) => {
        var pre = new PreJSON(null, (msg) => {
          if (def.promise.isPending()) {
            def.resolve(msg);
          }
          this._clearPending(service, null, msg);
          if (_.get(msg, 'node')) {
            _.unset(this._sidMap, sid);
            _.unset(this._srvMap, service);
            _.attempt(ctrl.abort);
          }
          this.emit('service:' + service, msg);
        });
        ctrl.onmessage = pre.add.bind(pre);
        ctrl.resume()
        .finally(() => {
          if (def.promise.isPending()) {
            def.reject(new NetworkError('remote connection closed'));
          }
        });
        return def.promise;
      });
    });
  }

  /**
   * @param  {redim.MessageEvent} data
   */
  _onMessage(data) {
    try {
      const msg = JSON.parse(_.toString(data.data));
      const service = this._sidMap[msg.sid];
      if (service) {
        _.set(this._srvMap, [ service, 'node' ], msg);
        this._clearPending(service, null, msg);
        this.emit('service:' + service, msg);
      }
      else {
        debug('dns error: no subscription for:', msg);
      }
    }
    catch (err) {
      debug('dns error:', err);
    }
  }

  /**
   * @param {string} service
   * @param {?Error=} err
   * @param {?redim.NodeInfo=} info
   */
  _clearPending(service, err, info) {
    var reqs = _.get(this._srvMap, [ service, 'reqs' ]);
    _.set(this._srvMap, [ service, 'reqs' ], []);
    if (err) {
      _.forEach(reqs, (d) => d.reject(err));
    }
    else {
      _.forEach(reqs, (d) => d.resolve(info || { node: null }));
    }
  }

  /**
   * @param  {DnsClient|string} dns
   * @return {DnsClient}
   */
  static wrap(dns) {
    if (dns instanceof DnsClient) {
      dns.ref();
      return dns;
    }
    else {
      return new DnsClient(dns);
    }
  }
}

module.exports = DnsClient;
