// @ts-check

class DimError extends Error {}
DimError.prototype.name = DimError.name;

class Timeout extends DimError {}
Timeout.prototype.name = Timeout.name;

class NotFound extends DimError {}
NotFound.prototype.name = NotFound.name;

class InvalidParam extends DimError {}
InvalidParam.prototype.name = InvalidParam.name;

class NotSupported extends DimError {}
NotSupported.prototype.name = NotSupported.name;

class NetworkError extends DimError {}
NetworkError.prototype.name = NetworkError.name;

module.exports = {
  DimError, Timeout, NotFound, InvalidParam, NotSupported, NetworkError
};
