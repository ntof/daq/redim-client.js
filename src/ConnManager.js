// @ts-check
const
  q = require('q'),
  _ = require('lodash'),
  debug = require('debug')('redim:conn'),
  support = require('./support'),
  DimError = require('./DimError');

/**
 * @typedef {import('./types').WebSocket} WebSocket
 */

/*::
import type { fetch$Ctrl } from './support'

declare type url = string;
*/

/**
  @details Open websockets only one by one to work-around cern-sso-proxy
  openshift container bug (when websockets are opened in parallel session is
    broken)

  Also share websocket for a given url, client must then call releaseWebSocket
  once done.
 */
class ConnManager {
  constructor() {
    /** @type {{ [url: string]: Q.Promise<WebSocket> }} */
    this._wsPool = {};
    /** @type {Q.Promise<void>} */
    this._wsProm = q();
    /** @type {?NodeJS.Timeout} */
    this._timer = null;
    /** @type {?NodeJS.Timeout} */
    this._timerKeepAlive =
      setInterval(this.keepAlive.bind(this), ConnManager.KEEPALIVE_DELAY);
  }

  keepAlive() {
    _.forEach(this._wsPool, (/** @type {Q.Promise<WebSocket>} */ wsProm) => {
      if (wsProm && wsProm.isFulfilled()) {
        wsProm.tap((ws) => {
          if (ws.readyState === WebSocket.OPEN) {
            ws.send(JSON.stringify(ConnManager.KEEPALIVE_PAYLOAD));
          }
        });
      }
    });
  }

  release() {
    if (this._timerKeepAlive) {
      clearInterval(this._timerKeepAlive);
      this._timerKeepAlive = null;
    }

    if (this._timer) {
      clearTimeout(this._timer);
      this._timer = null;
    }
  }

  clear() {
    _.forEach(this._wsPool, (
      /** @type {Q.Promise<WebSocket>} */ wsProm,
      /** @type {string} */ url) => {
      if (wsProm.isPending() || wsProm.isFulfilled()) {
        debug('clear: dropping an opened websocket on', url);
      }
      this._wsPool = {};
    });
  }

  cleanup() {
    debug('cleaning ws pool');
    _.forEach(this._wsPool, (
      /** @type {Q.Promise<WebSocket>} */ wsProm,
      /** @type {string} */ url) => {
      if (!wsProm || wsProm.isRejected()) {
        _.unset(this._wsPool, url);
      }
    });
  }

  _delayCleanup() {
    debug('delaying ws pool cleanup');
    if (this._timer) {
      return;
    }

    this._timer = setTimeout(() => {
      this._timer = null;
      this.cleanup();
    }, ConnManager.CLEANUP_DELAY);
  }

  /**
   * @param  {string} url
   * @param  {?boolean=} noPool
   * @return {Q.Promise<WebSocket>}
   */
  webSocket(url, noPool) {
    /* $FlowIgnore */
    if (!support.WebSocket) {
      debug('no websocket support');
      return q.reject(new DimError.NotSupported('no websocket support'));
    }
    debug('requesting', url);
    var wsProm = noPool ? undefined : this._wsPool[url];
    if (wsProm) {
      return wsProm /* $FlowIgnore */
      .tap(function(ws) { ++ws._ref; });
    }

    /** @type {Q.Deferred<WebSocket>} */
    var def = q.defer();
    this._wsProm = this._wsProm
    .finally(() => {
      var wsProm = noPool ? undefined : this._wsPool[url];
      if (wsProm) {
        return wsProm
        .then(/* $FlowIgnore */
          function(ws) { ++ws._ref; def.resolve(ws); },
          def.reject.bind(def));
      }

      var ws = new support.WebSocket(url);
      debug('connecting', url);
      if (!noPool) {
        this._wsPool[url] = def.promise;
        /* $FlowIgnore */
        ws._ref = 1;
      }
      ws.onopen = def.resolve.bind(def, ws);
      ws.onerror = () => {
        this._delayCleanup();
        def.reject(new DimError.NetworkError('websocket error'));
      };
      return def.promise;
    }).catch(_.noop);
    return def.promise;
  }

  /**
   * @param  {WebSocket} ws
   */
  releaseWebSocket(ws) {
    if (!ws) { return; }
    /* $FlowIgnore */
    if (--ws._ref <= 0) {
      delete this._wsPool[ws.url];
      debug('releasing', ws.url);
      ws.close(1000);
    }
  }
}

ConnManager.CLEANUP_DELAY = 15000;
ConnManager.KEEPALIVE_DELAY = 10000;
ConnManager.KEEPALIVE_PAYLOAD = { keepAlive: true };

module.exports = new ConnManager();
