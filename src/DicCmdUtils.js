// @ts-check
const
  _ = require('lodash'),

  xml = require('./xml');

/** @enum {number} */
const Status = {
  OK: 2,
  ERROR: 3
};

/** @type {DOMParser} */
let xmlParser;

class DicCmdUtils {
  /**
   * @return {DOMParser} [description]
   */
  static getXmlParser() {
    if (!xmlParser) {
      xmlParser = new xml.DOMParser();
    }
    return xmlParser;
  }

  /**
   * @param {string} strKey
   * @param {any} args
   * @return {Element}
   */
  static generateXmlCommand(strKey, args) {
    var root = xml.createDocument('command').documentElement;
    if (!root) { throw 'internal error'; }
    root.setAttribute('key', strKey);
    xml.fromJs(root, args);
    return root;
  }

  /**
   * @param {string} value
   * @param {string} strKey
   * @return {redim.DicXmlCmd.Reply|undefined}
   */
  static filterXmlReply(value, strKey) {
    if (!value) { return undefined; }
    // eslint-disable-next-line max-len
    const root = this.getXmlParser().parseFromString(value, 'text/xml').documentElement;
    if (!root) { return undefined; }

    const key = root.getAttribute('key');
    if (key === strKey || key === "-1") {
      var ret = {
        key: _.toNumber(key),
        status: _.toNumber(root.getAttribute('status')),
        error: _.toNumber(root.getAttribute('error')),
        message: xml.getText(root),
        xml: root
      };
      if (key === "-1") {
        // @ts-ignore
        delete ret.key;
        ret.message = root.getAttribute('message') || '';
      }
      return ret;
    }
    return undefined;
  }

  /**
   * @param  {?redim.DicXmlCmd.Reply=} reply
   * @return {?redim.DicXmlCmd.Reply}
   */
  static checkXmlReply(reply) {
    var status = _.get(reply, 'status');
    if (status === Status.OK) {
      // @ts-ignore: status would be undefined
      return reply;
    }
    else if (status === Status.ERROR) {
      throw reply;
    }
    else {
      throw { status: Status.ERROR, error: -1, message: 'unkown error' };
    }
  }

  /**
   * @param {number} error
   * @param {string} message
   * @return {redim.DicXmlCmd.Reply}
   */
  static createError(error, message) {
    return { status: Status.ERROR, error: error, message: message };
  }
}

DicCmdUtils.Status = Status;

module.exports = DicCmdUtils;
