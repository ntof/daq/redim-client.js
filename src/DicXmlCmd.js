// @ts-check
const
  q = require('q'),
  _ = require('lodash'),

  { EventEmitter } = require('events'),
  DimError = require('./DimError'),
  DicCmd = require('./DicCmd'),
  DicCmdUtils = require('./DicCmdUtils'),
  DicValue = require('./DicValue'),
  xml = require('./xml'),
  { getNextKey } = require('./utils');

/**
 * @typedef {import('./DnsClient')} DnsClient
 */

/**
 * @template Args=any
 * @template Return=any
 */
class DicXmlCmd extends EventEmitter {
  /**
   * @param {string} service
   * @param {?redim.DicXmlCmd.Options} options [description]
   * @param {string|DnsClient|redim.NodeInfo} dns     [description]
   */
  constructor(service, options, dns) {
    super();
    this.service = service;
    this.closed = false;

    var params = _.assign({ timer: 0, definition: 'C', watch: true, sid: 1,
      initial: true }, _.pick(options, [ 'proxy', 'timeout' ]));
    /** @type {DicValue<string>} */
    this.value = new DicValue(service + '/Ack', params, dns);
  }

  close() {
    this.closed = true;
    this.value.close();
    this.emit('close');
  }

  /**
   * @param {Args} args
   * @param {?number=} key
   * @param {?number=} timeout
   * @return {Q.Promise<Return>}
   */
  invoke(args, key, timeout) {
    var timeoutVal = _.defaultTo(timeout, DicXmlCmd.TIMEOUT);
    var strKey = _.toString(key || getNextKey());

    return this.value.promise()
    .timeout(timeoutVal * 1000, 'timeout')
    .then(() => {
      var root = DicCmdUtils.generateXmlCommand(strKey, args);

      var def = q.defer();
      /** @type {(rep: string) => void} */
      var cb = (rep) => {
        var reply = DicCmdUtils.filterXmlReply(rep, strKey);
        if (reply) {
          def.resolve(reply);
        }
      };
      this.value.on('value', cb);

      if (!this.value._url) {
        return q.reject(new DimError.NotFound('failed to connect service'));
      }
      return DicCmd.rawInvoke(this.value._url.slice(0, -9) + 'command',
        { name: this.service + '/Cmd',
          definition: 'C', data: xml.serializeToString(root) }, timeoutVal)
      .then(() => def.promise)
      .then((reply) => DicCmdUtils.checkXmlReply(reply))
      .timeout(timeoutVal * 1000, 'timeout')
      .finally(() => this.value.removeListener('value', cb));
    })
    .catch((err) => {
      if (err === 'timeout') {
        throw new DimError.Timeout('timeout on: ' + this.service);
      }
      throw err;
    });
  }

  /**
   * @template Args=any
   * @template Return=any
   * @param {string} service
   * @param {Args} args
   * @param {?redim.DicXmlCmd.Options} options
   * @param {DnsClient|redim.NodeInfo|string} dns
   * @return {Q.Promise<Return>}
   */
  static invoke(service, args, options, dns) {
    var cmd = new DicXmlCmd(service, options, dns);
    return cmd.invoke(args, _.get(options, 'key'))
    .finally(cmd.close.bind(cmd));
  }
}

DicXmlCmd.TIMEOUT = 3;

module.exports = DicXmlCmd;
