// @ts-check
const
  _ = require('lodash'),
  q = require('q'),
  axios = require('axios').default,

  ws = require('ws');

/** @type {(url: string) => Promise<redim.fetchCtrl>} */
var _fetch = function(url) {
  // @ts-ignore
  return q()
  .then(() => axios.get(url, { responseType: 'stream' }))
  .then((res) => {
    var abort = res.data.destroy.bind(res.data);
    var def = q.defer();
    /** @type {redim.fetchCtrl} */
    var ctrl = {
      abort: res.data.destroy.bind(res.data),
      resume: () => { res.data.resume(); return def.promise; },
      onmessage: null
    };
    res.data.on('data', (/** @type {any} */ data) => {
      // @ts-ignore
      if (_.attempt(ctrl.onmessage || _.noop, data) === true) {
        abort();
      }
    });
    res.data.once('close', () => {
      res.data.removeAllListeners();
      def.resolve();
    });
    return ctrl;
  });
};


module.exports = { WebSocket: ws, fetch: _fetch };
