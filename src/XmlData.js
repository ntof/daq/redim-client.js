// @ts-check

/** @type {typeof redim.XmlData.DataType} */
const DataType = {
  INVALID: -1,
  NESTED: 0,
  INT32: 1,
  DOUBLE: 2,
  STRING: 3,
  INT64: 4,
  ENUM: 5,
  INT8: 6,
  INT16: 7,
  FLOAT: 8,
  BOOL: 9,
  UINT32: 11,
  UINT64: 14,
  UINT8: 16,
  UINT16: 17
};

module.exports = { DataType };
