// @ts-check
const
  _ = require('lodash'),
  DicValue = require('./DicValue'),
  DicXmlValue = require('./DicXmlValue'),
  xml = require('./xml');

/**
 * @typedef {import('./DnsClient')} DnsClient
 */

/**
 * @param  {Element} error
 * @return {redim.XmlState.Error}
 */
function parseErr(error) {
  return {
    code: _.toNumber(error.getAttribute('code')),
    message: xml.getText(error)
  };
}

/**
 * @param {Element} value
 * @return {redim.XmlState.Value}
 */
function parseValue(value) {
  return {
    value: _.toNumber(value.getAttribute('value')),
    strValue: value.getAttribute('strValue') || ''
  };
}

/**
 * @extends DicXmlValue<redim.XmlState.State>
 */
// @ts-ignore
class DicXmlState extends DicXmlValue {
  /**
   * @param {redim.DicValue.Message<any>} rep
   */
  _setValue(rep) {
    if (rep && _.isString(rep.value)) {
      rep.value = DicXmlState.parse(rep.value);
    }
    super._setValue(rep);
  }

  /**
   * @param  {string} xmlsource
   * @return {redim.XmlState.State}
   */
  static parse(xmlsource) {
    var root = DicXmlValue.parse(xmlsource);

    if (!root) {
      /** @type {redim.XmlState.State} */
      return { value: NaN, strValue: '', errors: [], warnings: [],
        description: [] };
    }
    var warnings = _.flatMap(root.getElementsByTagName('warnings'),
      (tag) => _.map(tag.getElementsByTagName('warning'),
        /** @type {(e: any) => Element} */ (_.identity)));

    var errors = _.flatMap(root.getElementsByTagName('errors'),
      (tag) => _.map(tag.getElementsByTagName('error'),
        /** @type {(e: any) => Element} */ (_.identity)));

    return {
      value: _.toNumber(root.getAttribute('value')),
      strValue: root.getAttribute('strValue') || '',
      errors: _.map(errors, parseErr),
      warnings: _.map(warnings, parseErr),
      description: _.map(root.getElementsByTagName('value'), parseValue)
    };
  }

  /**
   * @param {string} service
   * @param {?redim.DicValue.Options} options
   * @param {string|DnsClient|redim.NodeInfo} dns
   * @return {Q.Promise<redim.XmlState.State>}
   */
  static get(service, options, dns) {
    return DicValue.get(service, options, dns)
    .then((ret) => DicXmlState.parse(ret || ''));
  }
}

module.exports = DicXmlState;
