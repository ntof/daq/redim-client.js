// @ts-check
const
  { EventEmitter } = require('events'),
  debug = require('debug')('redim:client:value'),
  _ = require('lodash'),
  q = require('q'),
  axios = require('axios').default,

  DnsRegistry = require('./DnsRegistry'),
  PreJSON = require('./PreJSON'),
  utils = require('./utils'),
  { NetworkError, NotSupported } = require('./DimError'),
  support = require('./support'),
  ConnManager = require('./ConnManager');

/**
 * @typedef {import('./types').WebSocket} WebSocket
 * @typedef {import('./DnsClient')} DnsClient
 */

/**
 * @template T=any
 * @brief DIM Client Value wrapper
 * @details emits:
 *  - 'value' whenever value is updated
 * @extends EventEmitter
 */
class DicValue extends EventEmitter {
  /**
   * @param {string} service
   * @param {?redim.DicValue.Options} options
   * @param {string|DnsClient|redim.NodeInfo} dns
   */
  constructor(service, options, dns) {
    super();
    _.bindAll(this, [ '_setNode', '_setDns', '_stream', '_ws' ]);
    this.service = service;
    /** @type {T|undefined} */
    this.value = undefined;
    this.closed = false;
    /** @type {?number} */
    this.timestamp = null;
    /** @type {?number} */
    this.quality = null;
    /** @type {?DnsClient} */
    this._dns = null;
    /** @type {?string} */
    this._url = null;

    /** @type {redim.DicValue.Params} */
    this._params = {
      name: service,
      watch: _.get(options, 'watch', true),
      initial: _.get(options, 'initial', true),
      timer: _.get(options, 'timer', 0),
      stamped: _.get(options, 'stamped', false)
    };
    if (_.has(options, 'definition')) {
      this._params.definition = _.get(options, 'definition');
    }
    /** @type {number} */
    this._timeout = _.get(options, 'timeout', DicValue.TIMEOUT);

    /** @type {string|undefined} */
    var proxy = _.get(options, 'proxy');
    this._baseUrl = (proxy) ? proxy : 'http://';
    if (!_.endsWith(this._baseUrl, '/')) {
      this._baseUrl += '/';
    }

    if (_.has(dns, 'node') && _.has(dns, 'port')) {
      this._setNode(/** @type {redim.NodeInfo} */ (dns));
    }
    else {
      this._setDns(/** @type {string|DnsClient} */ (dns));
    }
  }

  close() {
    this.closed = true;
    if (this._dns) {
      this._setDns(null);
    }
    _.attempt(this._abort || _.noop);
    this.emit('close');
  }

  isConnected() {
    return !!this._abort;
  }

  /**
   * @param  {redim.NodeNetInfo} nodeInfo
   * @return {?string}
   */
  _getUrl(nodeInfo) {
    if (!nodeInfo || !nodeInfo.node || !_.isNumber(nodeInfo.port)) {
      return null;
    }

    return `${this._baseUrl}${nodeInfo.node}:${nodeInfo.port}/dis/subscribe`;
  }

  /**
   * @param {?(string|DnsClient)} url
   * @return {boolean}
   */
  _setDns(url) {
    debug('setDns', _.get(url, 'url', url));
    var dns = DnsRegistry.get(url);
    if (dns && (dns === this._dns)) {
      debug('dns already set', url);
      dns.unref();
      return false;
    }
    else if (this._dns) {
      var old = this._dns;
      old.removeListener('service:' + this.service, this._setNode);
      old.unref();
    }
    this._dns = dns;
    if (dns) {
      dns.query(this.service)
      .then(
        (info) => {
          if (this._dns !== dns) { return; } // dns has changed, do not hook
          this._setNode(info);
          // @ts-ignore: checked above
          dns.on('service:' + this.service, this._setNode);
        },
        (err) => {
          if (this._dns !== dns) { return; } // dns has changed, do not hook
          // @ts-ignore: checked above
          dns.unref();
          this._dns = null;
          debug('dns error:', this.service, err);
          this.emit('error', err);
          this._setValue(undefined);
        }
      );
    }
    return true;
  }

  /**
   * @param {redim.NodeInfo} nodeInfo
   * @return {?PromiseLike<void>}
   */
  _setNode(nodeInfo) {
    debug('setNode', nodeInfo);
    var url = this._getUrl(nodeInfo);
    if (!url) {
      debug('invalid nodeInfo:', nodeInfo);
      return null;
    }
    else if (this._url === url && this.isConnected()) {
      debug('already connected:', url);
      return null;
    }
    this._url = url;
    if (_.has(nodeInfo, 'definition') && !_.has(this._params, 'definition')) {
      this._params.definition = _.get(nodeInfo, 'definition');
    }
    this._abort = this._setValue.bind(this, undefined);
    if (this._params.watch || this._params.timer) {
      this._params.sid = 1;

      return ConnManager.webSocket(utils.toWs(url))
      .then(this._ws, _.partial(this._stream, url))
      .catch((err) => {
        debug('service error:', this.service, err);
        /* $FlowIgnore */
        this.emit('error', utils.wrapError(NetworkError, err));
      })
      .finally(() => {
        debug('closed:', this.service);
        _.unset(this, '_abort');
        this._setValue(undefined);
        if (!this._dns) {
          this.close();
          return undefined;
        }
        else {
          debug('refreshing dns query');
          return this._dns.refreshQuery(this.service)
          .catch((e) => {
            if (_.get(e, 'name') === NotSupported.name) {
              /* reconnect dns */
              const dns = this._dns;
              this._setDns(null);
              this._setDns(dns);
            }
          });
        }
      });
    }
    else {
      return axios.get(url,
        { timeout: this._timeout, params: this._params,
          transformResponse: [ utils.parseReply ] })
      .then((ret) => this._setValue(ret.data))
      .catch((err) => {
        debug('service error:', this.service, err);
        /* $FlowIgnore */
        this.emit('error', utils.wrapError(NetworkError, err));
        this._setValue(undefined);
      })
      .finally(() => {
        debug('closed:', this.service);
        _.unset(this, '_abort');
        this.close();
      });
    }
  }

  /**
   * @param  {WebSocket} ws
   * @return {Q.Promise<void>}
   */
  _ws(ws) {
    var def = q.defer();
    debug('connected (ws):', this.service);
    if (!ws.req) { ws.req = {}; }
    if (!ws.sid) { ws.sid = 0; }
    this._params.sid = ++ws.sid;

    this._abort = () => {
      try {
        // @ts-ignore
        _.unset(ws, [ 'req', this._params.sid ]);
        ws.send(JSON.stringify({
          name: this._params.name, sid: this._params.sid,
          watch: false, initial: false, timer: 0
        }));
      }
      catch (err) { /* noop */}
      def.resolve();
    };

    var listeners = {
      'error': (/** @type {any} */ err) => {
        debug('ws error:', err);
        utils.rejectIfPending(def, err);
      },
      'close': (/** @type {{ code: number, reason: string }} */ event) => {
        if (_.get(event, 'code') !== 1000) {
          utils.rejectIfPending(def,
            new NetworkError(_.get(event, 'reason', 'network error')));
        }
        else {
          utils.resolveIfPending(def, undefined);
        }
      }
    };
    ws.req[this._params.sid] = (msg) => {
      if (_.isNil(_.get(msg, 'value'))) {
        return _.attempt(this._abort || _.noop); /* close current connection, wait for dns update */
      }
      return this._setValue(msg);
    };
    if (!ws.onmessage) {
      ws.onmessage = DicValue._wsGlobalHandler.bind(null, ws);
    }
    _.forEach(listeners, function(cb, event) {
      ws.addEventListener(event, cb);
    });
    ws.send(JSON.stringify(this._params));
    return def.promise
    .finally(() => {
      _.forEach(listeners, function(cb, event) {
        ws.removeEventListener(event, cb);
      });
      ConnManager.releaseWebSocket(ws);
    });
  }

  /**
   * @param {WebSocket} ws
   * @param {redim.MessageEvent} data
   */
  static _wsGlobalHandler(ws, data) {
    try {
      var msg = JSON.parse(_.toString(data.data));
      /* @ts-ignore */
      _.attempt(ws.req[msg.sid], msg);
    }
    catch (err) {
      debug('ws parse error:', err);
      ws.close(1007, 'failed to parse');
    }
  }

  /**
   * @param {string} url
   * @return {any}
   */
  _stream(url) {
    return support.fetch(url + '?' + utils.toQueryString(this._params))
    .then((ctrl) => {
      var pre = new PreJSON(null,
        (/** @type {redim.DicValue.Message<T>} */ msg) => {
          if (_.isNil(_.get(msg, 'value'))) {
            return _.attempt(ctrl.abort); /* close current connection, wait for dns update */
          }
          return this._setValue(msg);
        });
      ctrl.onmessage = pre.add.bind(pre);
      this._abort = ctrl.abort;
      return ctrl.resume();
    })
    // @ts-ignore is a q promise
    .tap(() => {
      if (this._dns) {
        this._dns.query(this.service).catch(_.noop); /* trigger a request */
      }
    });
  }

  /**
   * @param {redim.DicValue.Message<T>|undefined} data
   */
  _setValue(data) {
    this.value = _.get(data, 'value');
    if (_.has(data, 'timestamp')) {
      // @ts-ignore: checked above
      this.timestamp = data.timestamp;
      // @ts-ignore: checked above
      this.quality = data.quality;
    }
    debug('value set:', this.value);
    this.emit('value', this.value);
  }

  /**
   * @return {Q.Promise<T|undefined>}
   */
  promise() {
    if ((this.value === undefined) && (this._dns || this.isConnected())) {
      var def = q.defer();

      this.once('value', (value) => {
        utils.resolveIfPending(def, value);
        // Disconnect listener on 'error' preventing leaks
        this.removeListener('error', utils.rejectIfPending);
      });
      this.once('error', (err) => {
        utils.rejectIfPending(def, utils.wrapError(NetworkError, err));
        // Disconnect listener on 'value' preventing leaks
        this.removeListener('value', utils.resolveIfPending);
      });

      return def.promise;
    }
    else {
      return q(this.value);
    }
  }

  /**
   * @template K
   * @param {(value: T|undefined) => K|Promise<K>|Q.Promise<K>} fulfill
   * @param {(error: Error|string) => any} reject
   * @return {Q.Promise<K>}
   */
  then(fulfill, reject) {
    return this.promise().then(fulfill, reject);
  }

  /**
   * @template T=any
   * @param {string} service
   * @param {?redim.DicValue.Options} options
   * @param {string|DnsClient|redim.NodeInfo} dns
   * @return {Q.Promise<T|undefined>}
   */
  static get(service, options, dns) {
    var dic = new DicValue(service,
      _.assign(options, { watch: false, timer: 0, initial: true }), dns);
    return dic.promise();
  }
}
DicValue.TIMEOUT = 5000;

module.exports = DicValue;
