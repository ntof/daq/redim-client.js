// @ts-check

const
  _ = require('lodash'),
  debug = require('debug')('redim:client:servicesList'),
  DicValue  = require('./DicValue');

/**
 * @typedef {import('./DnsClient')} DnsClient
 * @typedef {import('./types').NodeInfo} NodeInfo
 */

/**
 * DIM Client Value for retrieving a list of services
 * @extends DicValue<{ [serviceName: string]: string }>
 */
// @ts-ignore: overridden 'get' property does not require service parameter
class DicServicesList extends DicValue {
  /**
  * create a specific DicValue object to retrieve information about services
  * @param {string} taskName
  * @param {?redim.DicValue.Options} options
  * @param {DnsClient|string|NodeInfo} dns
  */
  constructor(taskName, options, dns) {
    super(taskName + '/SERVICE_LIST', options, dns);
  }

  /**
   * Overridden method for service monitoring purpose
   * @param {any} rep
   */
  _setValue(rep) {
    if (_.has(rep, 'value')) {
      rep.value = update(rep.value, _.clone(this.value) || {});
    }
    super._setValue(rep);
  }

  /**
   * @brief retrieve services information once from a DIM Service Provider
   * @param {string} taskName
   * @param {?redim.DicValue.Options} options
   * @param {string|DnsClient|redim.NodeInfo} dns
   * @param {?number=} timeout
   * @return {Q.Promise<{ [serviceName: string]: string }|void>}
   */
  static get(taskName, options, dns, timeout = DicValue.TIMEOUT) {
    const opts = _.assign(options, { timeout });
    return DicValue.get(taskName + '/SERVICE_LIST', opts, dns)
    .then((ret) => {
      if (_.isNil(ret)) {
        return undefined;
      }
      else {
        return update(_.isBuffer(ret) ? _.toString(ret) : ret, {});
      }
    });
  }
}

/**
 * @brief Parse the list of services
 * @param {string} data
 * @param {{ [serviceName: string]: string }} db already registered services
 * @return { { [serviceName: string]: string } }
 */
function update(data, db) {
  data = _.split(data, '\0')[0]; /* remove any null char */
  let removing = false; /* make this sticky */

  return _.transform(_.split(data, '\n'), function(ret, srvDef) {
    var srv = _.toString(srvDef);
    var idx = srv.indexOf('|');

    let name = srv.slice(0, idx);
    if (name[0] === '-' || name[0] === '+') {
      removing = (name[0] === '-');
      name = name.slice(1);
    }

    if (removing) {
      delete ret[name];
    }
    else {
      var def = (idx !== -1) ? srv.slice(idx + 1) : null;
      if (_.isNil(def)) {
        debug('failed to parse definition:', srv);
      }
      else {
        ret[name] = def;
      }
    }
  }, db);
}

module.exports = {
  serviceList: DicServicesList.get,
  DicServicesList
};
