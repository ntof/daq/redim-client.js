// @ts-check
const
  DicValue = require('./DicValue'),
  DnsClient = require('./DnsClient'),
  DicCmd = require('./DicCmd'),
  DicDis = require('./DicDis'),
  DicDns = require('./DicDns'),
  DimError = require('./DimError'),
  DicCmdUtils = require('./DicCmdUtils'),

  DicXmlState = require('./DicXmlState'),
  DicXmlCmd = require('./DicXmlCmd'),
  DicXmlRpc = require('./DicXmlRpc'),
  DicXmlDataSet = require('./DicXmlDataSet'),
  DicXmlParams = require('./DicXmlParams'),
  DicXmlValue = require('./DicXmlValue'),
  XmlData = require('./XmlData'),
  xml = require('./xml');

module.exports = { DicValue, DnsClient, DicCmd, DicDis, DicDns, DimError,
  DicCmdUtils, DicXmlState, DicXmlCmd, DicXmlRpc, DicXmlDataSet,
  DicXmlParams, DicXmlValue,
  xml, XmlData };
