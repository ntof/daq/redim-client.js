// @ts-check
/*
** Copyright (C) 2016 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2016-11-01T18:24:40+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  debug = require('debug')('json:pre'),
  _ = require('lodash'),
  { EventEmitter } = require('events');

/**
 * @typedef {{
 *  inString: boolean,
 *  escape: boolean,
 *  sz: number,
 *  block: ?{ start: number, end: number },
 *  depth: number
 * }} Context
 */

/**
 * PreJSON stream json parser.
 * @extends EventEmitter
 */
class PreJSON extends EventEmitter {
  /**
   * @param {?number=} bufSize
   * @param {?((item: any) => any)=} callback
   */
  constructor(bufSize, callback) {
    super();
    this.callback = callback;
    this.buffer = Buffer.allocUnsafe(_.defaultTo(bufSize, 2048));
    /** @type {Context} */
    this.ctx = { inString: false, escape: false, sz: 0, block: null, depth: 0 };
  }

  /**
   * @param {Buffer|Array<number>} buffer
   */
  add(buffer) {
    for (var i = 0; i < buffer.length; ++i) {
      if (this._preParse(buffer[i])) {
        this._parse();
      }
    }
  }

  /**
   * @param {number} c
   * @param {Context} ctx
   */
  _preParseString(c, ctx) {
    if (ctx.escape) {
      ctx.escape = false;
    }
    else if (c === 0x5C /* \\ */) {
      ctx.escape = true;
    }
    else if (c === 0x22 /* '"' */) {
      ctx.inString = false;
    }
  }

  /**
   * @param {number} c
   * @param {Context} ctx
   */
  _preParseBlkStart(c, ctx) {
    if (c === 0x7b /* '{' */) {
      ctx.block = { start: 0x7b, end: 0x7d };
      ctx.depth = 1;
    }
    // /* uncomment this to accept arrays */
    // else if (c === 0x5b /* '[' */) {
    //   ctx.block = { start: 0x5b, end: 0x5d };
    //   ctx.depth = 1;
    // }
  }

  /**
   * @param {number} c
   * @param {Context} ctx
   * @return {boolean}
   */
  _preParseBlk(c, ctx) {
    /* @ts-ignore: tested in _preParse */
    if (c === ctx.block.start) {
      ctx.depth += 1;
    } /* @ts-ignore: tested in _preParse */
    else if (c === ctx.block.end) {
      ctx.depth -= 1;
    }
    return ctx.depth === 0;
  }

  /**
   * @param  {number} c
   * @return {boolean}
   */
  _preParse(c) {
    var ret = false;
    if (this.ctx.inString) {
      this._preParseString(c, this.ctx);
    }
    else if (_.includes([ 0x20, 0x09, 0x0A ], c)) {
      /* filtered out */
      return ret;
    }
    else {
      if (this.ctx.depth === 0) {
        this._preParseBlkStart(c, this.ctx);
      }
      else {
        ret = this._preParseBlk(c, this.ctx);
      }
      this.ctx.inString = (c === 0x22 /* '"' */);
    }
    this._resize();
    this.buffer[this.ctx.sz++] = c;
    return ret;
  }

  _resize() {
    if (this.ctx.sz >= this.buffer.length) {
      var old = this.buffer;
      this.buffer = Buffer.alloc(this.buffer.length * 2);
      old.copy(this.buffer, 0, 0, this.ctx.sz);
    }
  }

  /**
   * @param {any} msg [description]
   */
  error(msg) {
    debug('error:', msg);
  }

  _parse() {
    try {
      var ret = JSON.parse(_.toString(this.buffer.slice(0, this.ctx.sz)));
      debug('new item:', ret);
      this.emit('item', ret);
      _.attempt(this.callback || _.noop, ret);
    }
    catch (err) {
      this.error(err);
    }
    this.ctx = { inString: false, escape: false, sz: 0, block: null, depth: 0 };
  }
}

module.exports = PreJSON;
