// @ts-check
const
  _ = require('lodash'),
  { browserInfo, isWebWorker } = require('./utils');

/** @type {DOMImplementation} */
var _implem;
/** @type {XMLSerializer} */
var _serializer;
/** @type {typeof DOMParser} */
var _domParser;
if (browserInfo && !isWebWorker()) {
  _implem = document.implementation;
  _serializer = new XMLSerializer();
  _domParser = DOMParser;
}
else {
  const xmldom = require('xmldom'); /* eslint-disable-line global-require */
  _implem = new xmldom.DOMImplementation();
  _serializer = new xmldom.XMLSerializer();
  _domParser = xmldom.DOMParser;
}

const NodeType = {
  ELEMENT_TYPE: 1,
  TEXT_NODE: 3
};

/**
 * @param {string} tag
 * @return {Document}
 */
function createDocument(tag) {
  return _implem.createDocument('', tag, null);
}

/**
 * @param {Node} node
 * @return {string}
 */
function serializeToString(node) {
  return _serializer.serializeToString(node);
}

/**
 * @param {Element} node
 * @param {redim.XmlJsAttrList} attrs
 */
function fromJsAttrs(node, attrs) {
  _.forEach(attrs, function(value, key) {
    node.setAttribute(key, _.toString(value));
  });
}

/**
 * @brief generate xml document from javascript object
 * @param  {Element} node
 * @param {redim.XmlJsObject} args
 * @return {Element}
 */
function fromJs(node, args) {
  if (_.isObjectLike(args)) {
    _.forEach(args, function(value, key) {
      if (key === '$') {
        fromJsAttrs(node, value);
      }
      else if (key === '$textContent') {
        const elt = node.ownerDocument.createTextNode(_.toString(value));
        node.appendChild(elt);
      }
      else if (_.isArray(value)) {
        _.forEach(value, function(value) {
          const elt = node.ownerDocument.createElement(key);
          fromJs(elt, value);
          node.appendChild(elt);
        });
      }
      else {
        const elt = node.ownerDocument.createElement(key);
        fromJs(elt, value);
        node.appendChild(elt);
      }
    });
  }
  else {
    const elt = node.ownerDocument.createTextNode(_.toString(args));
    node.appendChild(elt);
  }
  return node;
}

/**
 * @brief convert xml to json
 * @param {Element} node
 * @return {redim.XmlJsObject}
 */
function toJs(node) {
  /** @type {redim.XmlJsObject} */
  var ret = {};
  var text = '';

  _.forEach(node.childNodes, function(sub) {
    if (sub.nodeType === NodeType.ELEMENT_TYPE) {
      const elt = /** @type {Element} */ (sub);
      var parent = _.get(ret, elt.tagName);
      if (parent) {
        if (_.isArray(parent)) {
          parent.push(toJs(elt));
        }
        else {
          ret[elt.tagName] = [ parent, toJs(elt) ];
        }
      }
      else {
        ret[elt.tagName] = toJs(elt);
      }
    }
    else if (sub.nodeType === NodeType.TEXT_NODE) {
      text += /** @type {Text} */ (sub).data;
    }
  });
  if (node.attributes.length !== 0) {
    /** @type {redim.XmlJsAttrList} */
    var attrs = {};
    _.forEach(node.attributes, function(a) { attrs[a.name] = a.value; });
    ret['$'] = attrs;
  }
  if (!_.isEmpty(text)) {
    if (_.isEmpty(ret)) {
      // @ts-ignore
      ret = text;
    }
    else {
      ret['$textContent'] = text;
    }
  }
  return ret;
}

/**
 * @brief get text from an XML element
 * @details iterates on childs to retrieve data from text nodes
 * @param {Element} node
 */
function getText(node) {
  var text = '';
  _.forEach(node.childNodes, function(sub) {
    if (sub.nodeType === NodeType.TEXT_NODE) {
      text += /** @type {Text} */ (sub).data;
    }
  });
  return text;
}

/**
 * @brief get specific child of an XML element
 * @details iterates on children to retrieve node with specific tag
 * @param {Node|Element} node
 * @param {string} tag
 * @return {Array<Element>}
 */
function getChildNodes(node, tag) {
  /** @type {Array<Element>} */
  const children = [];
  if (_.isNil(node)) { return children; }
  // eslint-disable-next-line guard-for-in
  for (const child in node.childNodes) {
    const index = _.toNumber(child);
    if (_.isNaN(index)) { continue; }
    if (node.childNodes[index].nodeType === NodeType.ELEMENT_TYPE &&
    /** @type {Element} */ (node.childNodes[index]).tagName === tag) {
      children.push(/** @type {Element} */ (node.childNodes[index]));
    }
  }
  return children;
}

module.exports = {
  DOMParser: _domParser,
  createDocument,
  serializeToString,
  fromJs,
  toJs,
  getText,
  getChildNodes
};
