// @ts-check

const
  { DimError } = require('./DimError'),
  q = require('q'),
  _ = require('lodash'),
  Bowser = require('bowser');

/**
 * @param {string} url
 * @return {string}
 */
function toWs(url) {
  if (_.startsWith(url, 'http://')) {
    return 'ws://' + url.slice(7);
  }
  else if (_.startsWith(url, 'https://')) {
    return 'wss://' + url.slice(8);
  }
  else {
    return url;
  }
}

/**
 * @param {any} data
 * @return {any}
 */
function parseReply(data) {
  if (typeof data === 'string') {
    try {
      data = JSON.parse(data);
    }
    catch (e) {
      return undefined;
    }
  }
  return data;
}

/**
 * @param {{ [key: string]: any }} params
 * @return {string}
 */
function toQueryString(params) {
  return _.map(params,
    (v, k) => encodeURIComponent(k) + '=' + encodeURIComponent(v)).join('&');
}

const browserInfo = (typeof navigator !== 'undefined') ?
  Bowser.getParser(_.get(navigator, 'userAgent')) : undefined;

function isWebWorker() {
  return (typeof navigator !== 'undefined') &&
    (typeof document === 'undefined');
}

/**
 * @param {typeof DimError} Klass
 * @param {any} err
 * @return {DimError}
 */
function wrapError(Klass, err) {
  if (!(err instanceof DimError)) {
    return new Klass(_.hasIn(err, 'message') ? err.message : _.toString(err));
  }
  return err;
}

/**
 * @template T
 * @param {Q.Deferred<T>} pObj
 * @param {T} value
 */
function resolveIfPending(pObj, value) {
  if (pObj.promise.isPending()) {
    pObj.resolve(value);
  }
}

/**
 * @param {Q.Deferred<any>} pObj
 * @param {?DimError} err
 */
function rejectIfPending(pObj, err) {
  if (pObj.promise.isPending()) {
    pObj.reject(err);
  }
}

/**
 * @param {any} reason
 * @return {Q.Promise<any>}
 */
function reject(reason) {
  const ret = q.reject(reason);
  /*
    handle rejection at this level, calling then will create a new branch,
    but if promise is unused this one is left handled and won't assert.
   */
  ret.catch(_.noop);
  return ret;
}

let keyId = 0;
const getNextKey = () => ++keyId;

module.exports = {
  toWs, parseReply, toQueryString, browserInfo, isWebWorker,
  wrapError, resolveIfPending, rejectIfPending, reject, getNextKey
};
