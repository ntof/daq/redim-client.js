// @ts-check
const
  _ = require('lodash'),
  DnsClient = require('./DnsClient');
/*::
  declare type $DnsUrl = string
*/

class DnsRegistry {
  constructor() {
    /** @type {{ [dnsUrl: string]: DnsClient }} */
    this.registry = {};
  }

  /**
   * @param {DnsClient} dns
   * @return {boolean}
   */
  _register(dns) {
    if (dns.closed || _.has(this.registry, dns.url)) {
      return false;
    }
    else {
      var url = dns.url;
      this.registry[url] = dns;
      dns.once('close', () => _.unset(this.registry, url));
      return true;
    }
  }

  /**
   * @param {string} url
   * @return {DnsClient}
   */
  _acquire(url) {
    var dns = this.registry[url];
    if (dns) {
      dns.ref();
      return dns;
    }
    else {
      dns = new DnsClient(url);
      this._register(dns);
      return dns;
    }
  }

  /**
   * @param {?(string|DnsClient)} dns
   * @return {?DnsClient}
   */
  get(dns) {
    if (!dns) { return null; }
    else if (dns instanceof DnsClient) {
      this._register(dns);
      dns.ref();
      return dns;
    }
    else {
      return this._acquire(/** @type {string} */ (dns));
    }
  }
}

module.exports = new DnsRegistry();
