// @ts-check
const
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  server = require('@cern/karma-server-side'),
  utils = require('./utils'),
  { DicDns } = require('../src');

describe('DicDns', function() {
  var env;
  var token;

  beforeEach(function() {
    this.timeout(15000);
    return utils.server.lock()
    .then((t) => { token = t; })
    .then(() => server.run(function() {
      /*:: var serverRequire = require */
      const { DnsProxy, DisProxy } = serverRequire('@ntof/redim-dim');
      const { DnsServer /*: DnsServer */ } = serverRequire('@cern/dim');
      const utils = serverRequire('./test/server_utils');

      this.env = {};
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer('127.0.0.1', 0);
        return this.env.dns.listen();
      })
      .then(() => {
        DnsProxy.register(this.env.app);
        DisProxy.register(this.env.app);
        var addr = this.env.server.address();
        return {
          dns: { port: this.env.dns._conn.port },
          proxyUrl: `http://127.0.0.1:${addr.port}`,
          dnsUrl: `http://127.0.0.1:${addr.port}` +
          `/127.0.0.1:${this.env.dns._conn.port}/dns/query`
        };
      });
    }))
    .then((ret) => (env = ret));
  });

  afterEach(function() {
    return utils.server.unlock(token)
    .then(() => server.run(function() {
      if (this.env) {
        this.env.dns.close();
        this.env.server.close();
        this.env = null;
      }
    }));
  });

  it('serves SERVICE_INFO', function() {
    return DicDns.serviceInfo('*', { proxy: env.proxyUrl }, env.dnsUrl)
    .then((ret) => {
      expect(ret).to.deep.equal({
        'DIS_DNS/SERVER_LIST': 'C|',
        'DIS_DNS/KILL_SERVERS': 'I|CMD',
        'DIS_DNS/SERVICE_INFO': 'C,C|RPC'
      });
    });
  });

  it('supports wildcard prefix', function() {
    return DicDns.serviceInfo('*_LIST', { proxy: env.proxyUrl }, env.dnsUrl)
    .then((ret) => expect(ret).to.deep.equal({
      'DIS_DNS/SERVER_LIST': 'C|'
    }));
  });

  it('supports wildcard postfix', function() {
    return DicDns.serviceInfo('DIS_DNS/SERV*', { proxy: env.proxyUrl },
      env.dnsUrl)
    .then((ret) => expect(ret).to.deep.equal({
      'DIS_DNS/SERVER_LIST': 'C|',
      'DIS_DNS/SERVICE_INFO': 'C,C|RPC'
    }));
  });

  it('supports fullname', function() {
    return DicDns.serviceInfo('DIS_DNS/KILL_SERVERS', { proxy: env.proxyUrl },
      env.dnsUrl)
    .then((ret) => expect(ret).to.deep.equal({
      'DIS_DNS/KILL_SERVERS': 'I|CMD'
    }));
  });

});
