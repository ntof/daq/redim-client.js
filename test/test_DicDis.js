const
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  server = require('@cern/karma-server-side'),
  utils = require('./utils'),
  { waitEvent } = require('./utils'),
  { DicDis } = require('../src');


describe('DicDis', function() {
  var env;
  var token;
  const servicesList = {
    "TEST/SERVICE_LIST": "C|",
    "TEST/VERSION_NUMBER": "L:1|",
    "TEST/CLIENT_LIST": "C|",
    "TEST/SET_EXIT_HANDLER": "L:1|CMD",
    "TEST/EXIT": "L:1|CMD"
  };

  beforeEach(function() {
    this.timeout(15000);
    return utils.server.lock()
    .then((t) => { token = t; })
    .then(() => server.run(function() {
      /*:: var serverRequire = require */
      const { DnsProxy, DisProxy } = serverRequire('@ntof/redim-dim');
      const { DnsServer /*: DnsServer */,
        DisNode /*: DisNode */, NodeInfo } = serverRequire('@cern/dim');
      const utils = serverRequire('./test/server_utils');

      this.env = {};
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer('127.0.0.1', 0);
        return this.env.dns.listen();
      })
      .then(() => {
        this.env.dis = new DisNode(NodeInfo.local('127.0.0.1', 0, 'TEST'));
        // this.env.node.
        return this.env.dis.register('127.0.0.1:' + this.env.dns._conn.port);
      })
      .then(() => {
        DnsProxy.register(this.env.app);
        DisProxy.register(this.env.app);
        var addr = this.env.server.address();
        return {
          node: { port: this.env.dis.info.port },
          dns: { port: this.env.dns._conn.port },
          proxyUrl: `http://127.0.0.1:${addr.port}`,
          dnsUrl: `http://127.0.0.1:${addr.port}` +
          `/127.0.0.1:${this.env.dns._conn.port}/dns/query`
        };
      });
    }))
    .then((ret) => (env = ret));
  });

  afterEach(function() {
    return utils.server.unlock(token)
    .then(() => server.run(function() {
      if (this.env) {
        this.env.dns.close();
        this.env.dis.close();
        this.env.server.close();
        this.env = null;
      }
    }));
  });

  it('can retrieve a services list', function() {
    return DicDis.serviceList('TEST', { proxy: env.proxyUrl }, env.dnsUrl)
    .then((list) => {
      return expect(list).to.be.deep.equal(servicesList);
    });
  });

  it('can monitor a services list', async function() {
    env.cli = new DicDis.DicServicesList('TEST', { proxy: env.proxyUrl },
      env.dnsUrl);

    let ret = await env.cli.promise();
    expect(ret).to.be.deep.equal(servicesList);

    env.cli.on('value', (val) => { ret = val; });
    // add service
    servicesList['test'] = "I|";

    return server.run(function() {
      this.env.dis.addService('test', 'I', 123);
    })
    .then(async () => {
      await waitEvent(env.cli, 'value');
      expect(ret).to.be.deep.equal(servicesList);
    })
    .finally(() => env.cli.close());
  });
});
