// @ts-check
const
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  q = require('q'),
  _ = require('lodash'),
  server = require('@cern/karma-server-side'),
  utils = require('./utils'),
  { DicValue } = require('../src'),
  ConnManager = require('../src/ConnManager');

/*::
import { DnsServer, DisNode } from '@cern/dim'
declare var serverRequire: (string) => any
*/

describe('ConnManager', function() {
  var env;
  var token;

  beforeEach(function() {
    this.timeout(15000); /* wait for other browsers to finish */
    return utils.server.lock()
    .then((t) => { token = t; })
    .then(() => server.run(function() {
      const { DnsServer /*: DnsServer */,
        DisNode /*: DisNode */ } = serverRequire('@cern/dim');
      const { DnsProxy, DisProxy } = serverRequire('@ntof/redim-dim');
      const utils = serverRequire('./test/server_utils');

      this.env = {};
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer('127.0.0.1', 0);
        return this.env.dns.listen();
      })
      .then(() => {
        this.env.node = new DisNode('127.0.0.1', 0);
        this.env.node.addService('SRV', 'I', 12);
        this.env.node.addService('SRV2', 'I', 13);
        return this.env.node.register('127.0.0.1:' + this.env.dns._conn.port);
      })
      .then(() => {
        DnsProxy.register(this.env.app);
        DisProxy.register(this.env.app);
        var addr = this.env.server.address();
        return {
          node: { port: this.env.node.info.port },
          dns: { port: this.env.dns._conn.port },
          proxyUrl: `http://127.0.0.1:${addr.port}`
        };
      });
    }))
    .then((ret) => (env = ret));
  });

  afterEach(function() {
    return utils.server.unlock(token)
    .then(() => server.run(function() {
      if (this.env) {
        this.env.dns.close();
        this.env.server.close();
        this.env.node.close();
        this.env = null;
      }
    }));
  });

  it('can share a websocket', function() {
    var dic = new DicValue('SRV',
      { definition: 'I', proxy: env.proxyUrl },
      { node: '127.0.0.1', port: env.node.port });
    var dic2 = new DicValue('SRV2',
      { definition: 'I', proxy: env.proxyUrl },
      { node: '127.0.0.1', port: env.node.port });

    return q()
    .then(() => dic.promise())
    .then(() => dic2.promise())
    .then(() => {
      expect(_.size(ConnManager._wsPool)).to.equal(1);
      expect(dic.value).to.equal(12);
      expect(dic2.value).to.equal(13);
      dic.close();
    })
    .then(() => {
      expect(_.size(ConnManager._wsPool)).to.equal(1);
      dic2.close();
    })
    .delay(100) /* DicValue.close is async */
    .then(() => {
      expect(_.size(ConnManager._wsPool)).to.equal(0);
    });
  });
});
