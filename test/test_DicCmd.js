// @ts-check
const
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  q = require('q'),
  server = require('@cern/karma-server-side'),
  utils = require('./utils'),
  { DicCmd, DimError } = require('../src');

/*::
import { DnsServer, DisNode } from '@cern/dim'
declare var serverRequire: (string) => any
*/

describe('DicCmd', function() {
  var env;
  var token;

  beforeEach(function() {
    this.timeout(15000); /* wait for other browsers to finish */
    return utils.server.lock()
    .then((t) => { token = t; })
    .then(() => server.run(function() {
      /*:: var serverRequire = require */
      const { DnsServer /*: DnsServer */, DisNode } = serverRequire('@cern/dim');
      const { DnsProxy, DisProxy } = serverRequire('@ntof/redim-dim');
      const utils = serverRequire('./test/server_utils');

      this.env = {};
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer('127.0.0.1', 0);
        return this.env.dns.listen();
      })
      .then(() => {
        this.env.node = new DisNode('127.0.0.1', 0);
        // this.env.node.
        return this.env.node.register('127.0.0.1:' + this.env.dns._conn.port);
      })
      .then(() => {
        DnsProxy.register(this.env.app);
        DisProxy.register(this.env.app);
        var addr = this.env.server.address();
        return {
          node: { port: this.env.node.info.port },
          dns: { port: this.env.dns._conn.port },
          proxyUrl: `http://127.0.0.1:${addr.port}`
        };
      });
    }))
    .then((ret) => (env = ret));
  });

  afterEach(function() {
    return utils.server.unlock(token)
    .then(() => server.run(function() {
      if (this.env) {
        this.env.dns.close();
        this.env.server.close();
        this.env.node.close();
        this.env = null;
      }
    }));
  });

  it('can call a cmd', function() {
    var dns = `${env.proxyUrl}/127.0.0.1:${env.dns.port}/dns/query`;

    return q()
    .then(() => server.run(function() {
      this.args = [];
      this.env.node.addCmd('CMD', 'I', (req) => this.args.push(req.value));
    }))
    .then(() => DicCmd.invoke('CMD', 42, { proxy: env.proxyUrl }, dns))
    .delay(100)
    .then(() => server.run(function() { return this.args; }))
    .then((args) => expect(args).to.deep.equal([ 42 ]));
  });

  it('can call a rpc', function() {
    var dns = `${env.proxyUrl}/127.0.0.1:${env.dns.port}/dns/query`;

    return q()
    .then(() => server.run(function() {
      this.env.node.addRpc('CMD', 'I,I|RPC', (req) => req.value + 1);
    }))
    .then(() => DicCmd.invoke('CMD/RpcIn', 41,
      { sid: 1, definition: 'I,I|RPC', proxy: env.proxyUrl }, dns))
    .then((res) => expect(res).to.deep.equal(
      { sid: 1, value: 42, name: 'CMD/RpcIn' }));
  });

  it('can timeout on rpc', function() {
    var dns = `${env.proxyUrl}/127.0.0.1:${env.dns.port}/dns/query`;

    return q()
    .then(() => server.run(function() {
      const q = serverRequire('q');
      this.env.node.addRpc('CMD', 'I,I|RPC', () => q(42).delay(1500));
    }))
    .then(() => DicCmd.invoke('CMD/RpcIn', 41,
      { timeout: 1, sid: 1, definition: 'I,I|RPC', proxy: env.proxyUrl }, dns))
    .then(
      () => expect.fail('should fail'),
      (err) => expect(err).to.be.instanceOf(DimError.Timeout));
  });

  it('reports an error if dns doesn\'t exist', function() {
    var dns = `${env.proxyUrl}/127.0.0.1:${env.dns.port}/WRONGDNS/dns/query`;

    return q()
    .then(() => server.run(function() {
      const q = serverRequire('q');
      this.env.node.addRpc('CMD', 'I,I|RPC', () => q(42).delay(1500));
    }))
    .then(() => DicCmd.invoke('CMD/RpcIn', 41,
      { timeout: 1, sid: 1, definition: 'I,I|RPC', proxy: env.proxyUrl }, dns))
    .then(
      () => expect.fail('should fail'),
      (err) => expect(err).to.be.instanceOf(DimError.NetworkError));
  });
});
