// @ts-check
const
  q = require('q'),
  _ = require('lodash'),
  debug = require('debug')('test'),
  express = require('express'),
  ews = require('express-ws'),
  chai = require('chai'),
  dirtyChai = require('dirty-chai');

chai.use(dirtyChai);

/*::
import type { $Application } from 'express';
*/

function createApp(env /*: { app: $Application<>, server: http$Server } */,
                   opts /*: ?{ noWebSocket: boolean } */) /*: Promise<void> */ {
  var def = q.defer();
  env.app = express();

  if (!_.get(opts, 'noWebSocket', false)) {
    ews(env.app);
  }

  var server = env.app.listen(0, function() {
    if (server) {
      env.server = server;
      def.resolve();
    }
    else {
      def.reject('failed to create Express server');
    }
  });
  return def.promise;
}

class Lock {
  /*::
    count: number
    locked: Array<QDeffered<any>>
  */
  constructor(count /*: number */) {
    this.count = count;
    this.locked = [];
  }

  wait() /*: Promise<void> */ {
    if (this.count === 0) {
      var defer = q.defer();
      this.locked.push(defer);
      debug('locked(%d)', _.size(this.locked));
      return defer.promise;
    }
    this.count -= 1;
    return q();
  }

  post() {
    if (!_.isEmpty(this.locked)) {
      var defer = this.locked.shift();
      _.defer(defer.resolve.bind(defer));
    }
    else {
      this.count += 1;
    }
  }
}

module.exports = {
  createApp, Lock
};
