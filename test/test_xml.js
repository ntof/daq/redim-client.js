// @ts-check
const
  { describe, it } = require('mocha'),
  { expect } = require('chai'),

  { xml } = require('../src');


describe('xml', function() {

  it('can inject json in xml', function() {
    var doc = xml.createDocument('root');
    var root = doc.documentElement;
    var js = {
      '$': { attr: 'attrValue' },
      '$textContent': 'blah',
      'elt1': 'test',
      'elt2': { '$': { attr: 'attrValue' }, 'elt3': 'data' }
    };

    if (!root) { throw 'fail'; }
    xml.fromJs(root, js);
    var str = xml.serializeToString(root);

    root = (new xml.DOMParser())
    .parseFromString(str, 'text/xml').documentElement;
    if (!root) { throw 'fail'; }
    expect(xml.toJs(root)).to.deep.equal(js);
  });

  it('can serialize numbers', function() {
    var doc = xml.createDocument('root');
    var root = doc.documentElement;

    if (!root) { throw 'fail'; }
    xml.fromJs(root, { test: 42, '$': { attr: 42 } });
    var str = xml.serializeToString(root);

    root = (new xml.DOMParser())
    .parseFromString(str, 'text/xml').documentElement;

    if (!root) { throw 'fail'; }
    expect(xml.toJs(root)).to.deep.equal({ test: "42", '$': { attr: '42' } });
  });

  it('can convert array of elements', function() {
    var doc = xml.createDocument('root');
    var root = doc.documentElement;
    var js = { elt1: [ { '$': { a: "1", b: "2" } }, { '$': { a: "3" } } ] };

    if (!root) { throw 'fail'; }
    xml.fromJs(root, js);
    var str = xml.serializeToString(root);
    expect(str).to.contain('<elt1 a');

    root = (new xml.DOMParser())
    .parseFromString(str, 'text/xml').documentElement;
    if (!root) { throw 'fail'; }
    expect(xml.toJs(root)).to.deep.equal(js);
  });
});
