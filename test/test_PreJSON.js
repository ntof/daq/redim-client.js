// @ts-check
const
  { describe, it } = require('mocha'),
  { expect } = require('chai'),

  PreJSON = require('../src/PreJSON');

describe('PreJSON', function() {
  it('parses json', function(done) {
    var pre = new PreJSON();

    pre.on('item', (item) => {
      expect(item).deep.equal({ sample: '{{{{\"' });
      done();
    });
    pre.add(Buffer.from('  { "sample": "{{{{\\"" }'));
  });

  it('parses partial items', function(done) {
    var pre = new PreJSON();

    pre.on('item', (item) => {
      expect(item).deep.equal({ partial: 'item' });
      done();
    });
    pre.add(Buffer.from('{ "par'));
    pre.add(Buffer.from('tial": '));
    pre.add(Buffer.from(' "item" }'));
  });

  it('parses all json types', function(done) {
    var pre = new PreJSON();
    var testItem = {
      array: [ { object: 'test' }, [ ], 12, true, false, "test" ],
      bool: true,
      sub: { }
    };

    pre.once('item', (item) => {
      expect(item).deep.equal(testItem);
      done();
    });
    pre.add(Buffer.from(JSON.stringify(testItem)));
  });

  it('can resize internal buffer', function(done) {
    var pre = new PreJSON(2);

    pre.once('item', (item) => {
      expect(item).deep.equal({ test: 42 });
      done();
    });
    pre.add(Buffer.from('{ "test": 42 }'));
  });

  it('disards invalid input', function() {
    var pre = new PreJSON();
    var called = 0;

    pre.on('item', () => { called += 1; });
    pre.add(Buffer.from('{ invalid }'));
    pre.add(Buffer.from('[ ]'));
    expect(called).deep.equal(0);
  });
});
