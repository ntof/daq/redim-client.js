// @ts-check
const
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  server = require('@cern/karma-server-side'),
  utils = require('./utils'),
  { DicValue } = require('../src');

/*::
import { DnsServer, DisNode } from '@cern/dim'
declare var serverRequire: (string) => any
*/

describe('DnsRegistry', function() {
  var env;
  var token;

  beforeEach(function() {
    this.timeout(15000); /* wait for other browsers to finish */
    return utils.server.lock()
    .then((t) => { token = t; })
    .then(() => server.run(function() {
      /* eslint-disable-next-line max-len */
      const { DnsServer /*: DnsServer */, DisNode /*: DisNode */ } = serverRequire('@cern/dim');
      const { DnsProxy, DisProxy } = serverRequire('@ntof/redim-dim');
      const utils = serverRequire('./test/server_utils');

      this.env = {};
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer('127.0.0.1', 0);
        return this.env.dns.listen();
      })
      .then(() => {
        this.env.node = new DisNode('127.0.0.1', 0);
        this.env.node.addService('SRV', 'I', 12);
        this.env.node.addService('SRV2', 'I', 21);
        return this.env.node.register('127.0.0.1:' + this.env.dns._conn.port);
      })
      .then(() => {
        DnsProxy.register(this.env.app);
        DisProxy.register(this.env.app);
        var addr = this.env.server.address();
        return {
          node: { port: this.env.node.info.port },
          dns: { port: this.env.dns._conn.port },
          proxyUrl: `http://127.0.0.1:${addr.port}`
        };
      });
    }))
    .tap((ret) => {
      ret.dns.url = `${ret.proxyUrl}/127.0.0.1:${ret.dns.port}/dns/query`;
    })
    .then((ret) => (env = ret));
  });

  afterEach(function() {
    return utils.server.unlock(token)
    .then(() => server.run(function() {
      if (this.env) {
        this.env.dns.close();
        this.env.server.close();
        this.env.node.close();
        this.env = null;
      }
    }));
  });

  it('can properly manages DnsRegistry refcount', function() {
    return DicValue.get('SRV', { proxy: env.proxyUrl }, env.dns.url)
    .then((value) => {
      expect(value).to.equal(12);
      /* here the first DNS is being destroyed, let's try to reuse it */
      return DicValue.get('SRV2', { proxy: env.proxyUrl }, env.dns.url);
    })
    .then((value) => expect(value).to.equal(21));
  });
});
