// @ts-check
const
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  _ = require('lodash'),
  q = require('q'),
  server = require('@cern/karma-server-side'),
  utils = require('./utils'),
  sinon = require('sinon'),
  { DnsClient, DimError } = require('../src');

/*::
import { DnsServer, DisNode } from '@cern/dim'
declare var serverRequire: (string) => any
*/

describe('DnsClient', function() {
  var env;
  var token;

  beforeEach(function() {
    this.timeout(15000); /* wait for other browsers to finish */
    return utils.server.lock()
    .then((t) => { token = t; })
    .then(() => server.run(function() {
      const { DnsServer /*: DnsServer */ } = serverRequire('@cern/dim');
      const { DnsProxy } = serverRequire('@ntof/redim-dim');
      const utils = serverRequire('./test/server_utils');

      this.env = {};
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer('127.0.0.1', 0);
        return this.env.dns.listen();
      })
      .then(() => {
        DnsProxy.register(this.env.app);
        var addr = this.env.server.address();
        var dnsUrl = '127.0.0.1:' + this.env.dns._conn.port;
        return `http://127.0.0.1:${addr.port}/${dnsUrl}/dns/query`;
      });
    }))
    .then((address) => { env = { address }; });
  });

  afterEach(function() {
    return utils.server.unlock(token)
    .then(() => server.run(function() {
      if (this.env) {
        this.env.dns.close();
        this.env.server.close();
        this.env = null;
      }
    }));
  });

  it('can query a dns', function() {
    var dns = new DnsClient(env.address);
    return dns.query('DIS_DNS/SERVICE_INFO/RpcIn')
    .then((info) => expect(info).to.deep.contain({
      node: '127.0.0.1', definition: 'C|CMD'
    }))
    .finally(() => dns.close());
  });

  it('uses cached information', function() {
    var dns = new DnsClient(env.address);

    return q()
    .then(() => dns._ws.then((ws) => sinon.spy(ws, 'send')))
    .then(() => dns.query('DIS_DNS/SERVICE_INFO/RpcIn'))
    .then(() => dns.query('DIS_DNS/SERVICE_INFO/RpcIn'))
    .then((info) => expect(info).to.have.property('node', '127.0.0.1'))
    .then(() => dns._ws.then((ws) => expect(ws.send.callCount).equal(1)))
    .finally(() => dns.close());
  });

  it('receives updates', function() {
    var dns = new DnsClient(env.address);

    return q()
    .then(() => dns.query('SRV'))
    .then((info) => expect(info).deep.equal({ node: null, sid: 1 }))
    .then(() => server.run(function() {
      const { DisNode /*: DisNode */ } = serverRequire('@cern/dim');

      var node = new DisNode('127.0.0.1', 0);
      node.addService('SRV', 'I', 12);
      node.register('127.0.0.1:' + this.env.dns._conn.port);
      this.env.dns.once('close', () => node.close());
    }))
    .then(_.constant(utils.waitEvent(dns, 'service:SRV', 2)))
    .then(() => dns.query('SRV'))
    .then((info) => expect(info).to.deep.contain(
      { node: '127.0.0.1', definition: 'I', sid: 1 }))
    .finally(() => dns.close());
  });

  it('fails to proxy to an unknown dns', function() {
    return server.run(function() {
      this.env.dns.close();
    })
    .then(() => {
      var dns = new DnsClient(env.address);
      return dns.query('SRV')
      .then(
        () => expect.fail('should fail'),
        (err) => expect(err).to.have.property('name',
          DimError.NetworkError.name))
      .finally(() => dns.close());
    });
  });
});
