// @ts-check
const
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  _ = require('lodash'),
  q = require('q'),
  server = require('@cern/karma-server-side'),
  utils = require('./utils'),
  utilsSrc = require('../src/utils'),
  sinon = require('sinon'),
  { DicValue, DnsClient, DimError } = require('../src'),
  ConnManager = require('../src/ConnManager');

/*::
import { DnsServer, DisNode } from '@cern/dim'
declare var serverRequire: (string) => any
*/

function describeDicValue(name, opts) {
  describe(name, function() {
    var env;
    var token;

    beforeEach(function() {
      this.timeout(15000); /* wait for other browsers to finish */
      return utils.server.lock()
      .then((t) => { token = t; })
      .then(() => server.run(opts, function(opts) {
        const { DnsServer /*: DnsServer */, DisNode /*: DisNode */ } =
        serverRequire('@cern/dim');
        const { DnsProxy, DisProxy } = serverRequire('@ntof/redim-dim');
        const utils = serverRequire('./test/server_utils');

        this.env = {};
        return utils.createApp(this.env, opts)
        .then(() => {
          this.env.dns = new DnsServer('127.0.0.1', 0);
          return this.env.dns.listen();
        })
        .then(() => {
          this.env.node = new DisNode('127.0.0.1', 0);
          this.env.node.addService('SRV', 'I', 12);
          return this.env.node.register('127.0.0.1:' + this.env.dns._conn.port);
        })
        .then(() => {
          DnsProxy.register(this.env.app);
          DisProxy.register(this.env.app);
          var addr = this.env.server.address();
          return {
            node: { port: this.env.node.info.port },
            dns: { port: this.env.dns._conn.port },
            proxyUrl: `http://127.0.0.1:${addr.port}`
          };
        });
      }))
      .then((ret) => (env = ret));
    });

    afterEach(function() {
      _.invoke(env.dic, 'close');
      return utils.server.unlock(token)
      .then(() => server.run(function() {
        if (this.env) {
          this.env.dns.close();
          this.env.server.close();
          this.env.node.close();
          this.env = null;
        }
      }));
    });

    it('can request a single value', function() {
      var dns =
      new DnsClient(`${env.proxyUrl}/127.0.0.1:${env.dns.port}/dns/query`);
      var dic = new DicValue('SRV',
        { definition: 'I', proxy: env.proxyUrl, watch: false }, dns);
      dns.unref();

      return dic.promise()
      .then((value) => expect(value).to.equal(12))
      .then(() => expect(dic.closed).to.be.true())
      .then(() => expect(dns.closed).to.be.true());
    });

    it('can request a single value (short-hand)', function() {
      var dns =
      new DnsClient(`${env.proxyUrl}/127.0.0.1:${env.dns.port}/dns/query`);

      return DicValue.get('SRV', { proxy: env.proxyUrl }, dns)
      .then((value) => expect(value).to.equal(12))
      .then(() => dns.unref())
      .then(() => expect(dns.closed).to.be.true());
    });

    it('can poll a service', function() {
      var dic = new DicValue('SRV',
        { definition: 'I', proxy: env.proxyUrl },
        { node: '127.0.0.1', port: env.node.port });

      return dic.promise()
      .then((value) => expect(value).to.equal(12))
      .finally(() => dic.close());
    });

    it('can poll a service (dns url)', function() {
      var dic = new DicValue('SRV',
        { proxy: env.proxyUrl },
        `${env.proxyUrl}/127.0.0.1:${env.dns.port}/dns/query`);

      return dic.promise()
      .then((value) => expect(value).to.equal(12))
      .finally(() => dic.close());
    });

    it('can poll a service (dns)', function() {
      var dns =
      new DnsClient(`${env.proxyUrl}/127.0.0.1:${env.dns.port}/dns/query`);
      var dic = new DicValue('SRV', { proxy: env.proxyUrl }, dns);
      dns.unref(); /* release our ref, DicValue has 1 now */

      return dic.promise()
      .then((value) => expect(value).to.equal(12))
      .finally(() => dic.close())
      /* dns should have been released by dic */
      .then(() => expect(dns).to.have.property('closed', true));
    });

    it('can detect service changes', function() {
      var dns =
        new DnsClient(`${env.proxyUrl}/127.0.0.1:${env.dns.port}/dns/query`);
      var dic = new DicValue('SRV2', { proxy: env.proxyUrl }, dns);
      var spy;

      return dns.query('SRV2')
      .then((info) => expect(info).to.deep.contain({ node: null }))
      .delay(10) /* wait for dic to resolve its info */
      .then(() => server.run(function() {
        this.env.node.addService('SRV2', 'I', 42);
      }))
      .then(() => dic.promise())
      .tap(() => {
        if (opts.noWebSocket) { return; }
        // Let's spy on websocket in order to catch unsub message later
        /* @ts-ignore: _url is private */
        const websock = ConnManager._wsPool[utilsSrc.toWs(dic._url)];
        websock.then((ws) => {
          spy = sinon.spy(ws, 'send');
        });
      })
      .then((value) => expect(value).to.equal(42))
      .then(() => server.run(function() {
        this.env.node.removeService('SRV2');
      }))
      .then(_.constant(utils.waitEvent(dic, 'value', 2, 2000)))
      .then((values) => expect(values).to.deep.equal([ 42, undefined ]))
      .then(() => server.run(function() {
        this.env.node.addService('SRV2', 'I', 33);
      }))
      .then(_.constant(utils.waitEvent(dic, 'value', 3, 2000)))
      .then((values) => expect(values).to.deep.equal([ 42, undefined, 33 ]))

      .then(() => server.run(function() {
        this.env.node.close();
      }))
      .then(_.constant(utils.waitEvent(dic, 'value', 4, 2000)))
      .then((values) => expect(values).to.deep.equal(
        [ 42, undefined, 33, undefined ]))
      .finally(() => dns.unref())
      .finally(() => dic.close())
      .then(() => {
        if (opts.noWebSocket) { return; }
        // Check unsubscribed message was sent on websocket
        const unsubMsg = {
          name: dic._params.name, sid: dic._params.sid,
          watch: false, initial: false, timer: 0
        };
        expect(JSON.parse(_.get(spy.args, [ 0, 0 ], {})))
        .to.deep.equal(unsubMsg);
      });
    });

    it('can refresh dns information', async function() {
      env.dic = new DicValue('SRV', { proxy: env.proxyUrl },
        `${env.proxyUrl}/127.0.0.1:${env.dns.port}/dns/query`);
      env.dic.on('error', _.noop);

      expect(await env.dic.promise()).to.be.equal(12);
      await server.run(function() {
        this.env.node.close();
      });
      await utils.waitForValue(() => env.dic.value, undefined);
      await server.run(function() {
        const { DisNode /*: DisNode */ } = serverRequire('@cern/dim');
        this.env.node = new DisNode('127.0.0.1', 0);
        this.env.node.addService('SRV', 'I', 42);
        return this.env.node.register('127.0.0.1:' + this.env.dns._conn.port)
        .then(() => null);
      });
      await utils.waitForValue(() => env.dic.value, 42);
    });

    it('throws network error if dns fails', function() {
      return q()
      .then(() => server.run(function() {
        this.env.dns.close();
      }))
      .then(() => {
        var dic = new DicValue('SRV',
          { definition: 'I', proxy: env.proxyUrl, watch: false },
          `${env.proxyUrl}/127.0.0.1:${env.dns.port}/dns/query`);
        return dic.promise()
        .then(() => { throw new Error('Promise should not be resolved'); })
        .catch((err) => {
          expect(err).to.have.property('name', DimError.NetworkError.name);
        });
      });
    });

    it('throws network error if node is unavailable (watch false)', function() {
      return q()
      .then(() => server.run(function() {
        this.env.node.close();
      }))
      .then(() => {
        var dic = new DicValue('SRV',
          { definition: 'I', proxy: env.proxyUrl, watch: false },
          { node: '127.0.0.1', port: env.node.port });
        return dic.promise()
        .then(() => { throw new Error('Promise should not be resolved'); })
        .catch((err) => {
          expect(err).to.have.property('name', DimError.NetworkError.name);
        });
      });
    });

    it('throws network error if node is unavailable (watch true)', function() {
      return q()
      .then(() => server.run(function() {
        this.env.node.close();
      }))
      .then(() => {
        var dic = new DicValue('SRV',
          { definition: 'I', proxy: env.proxyUrl, watch: true },
          { node: '127.0.0.1', port: env.node.port });
        return dic.promise()
        .then(() => { throw new Error('Promise should not be resolved'); })
        .catch((err) => {
          expect(err).to.have.property('name', DimError.NetworkError.name);
        });
      });
    });
  });
}

describeDicValue('DicValue', { noWebSocket: false });
describeDicValue('DicValue (noWebSocket)', { noWebSocket: true });
