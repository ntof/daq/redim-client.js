// @ts-check
const
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  q = require('q'),
  utils = require('./utils'),
  server = require('@cern/karma-server-side'),

  { DicXmlParams } = require('../src');

function  describeDicXmlParams(name, opts) {
  describe(name, function() {
    var env;
    var token;

    beforeEach(function() {
      this.timeout(15000);
      return utils.server.lock()
      .then((t) => { token = t; })
      .then(() => server.run(opts, function(opts) {
      /*:: var serverRequire = require */
        const { DisXmlNode } = serverRequire('@ntof/dim-xml');
        const { DnsProxy, DisProxy } = serverRequire('@ntof/redim-dim');
        const { DnsServer } = serverRequire('@cern/dim');
        const utils = serverRequire('./test/server_utils');

        this.env = {};
        return utils.createApp(this.env, opts)
        .then(() => {
          this.env.dns = new DnsServer('127.0.0.1', 0);
          return this.env.dns.listen();
        })
        .then(() => {
          this.env.node = new DisXmlNode('127.0.0.1', 0);
          return this.env.node.register('127.0.0.1:' + this.env.dns._conn.port);
        })
        .then(() => {
          DnsProxy.register(this.env.app);
          DisProxy.register(this.env.app);
          var addr = this.env.server.address();
          return {
            node: { port: this.env.node.info.port },
            dns: { port: this.env.dns._conn.port },
            proxyUrl: `http://127.0.0.1:${addr.port}`,
            dnsUrl: `http://127.0.0.1:${addr.port}` +
            `/127.0.0.1:${this.env.dns._conn.port}/dns/query`
          };
        });
      }))
      .then((ret) => (env = ret));
    });

    afterEach(function() {
      return utils.server.unlock(token)
      .then(() => server.run(function() {
        if (this.env) {
          this.env.dns.close();
          this.env.server.close();
          this.env.node.close();
          this.env = null;
        }
      }));
    });

    it('can use params', function() {
      var client =
        new DicXmlParams('/pwet', { proxy: env.proxyUrl }, env.dnsUrl);
      return server.run(function() {
      /*:: var serverRequire = require */
        const { XmlData } = serverRequire('@ntof/dim-xml');

        this.env.dataset = this.env.node.addXmlParams('/pwet');
        this.env.dataset.add({ name: 'sample1', unit: 'lux',
          type: XmlData.Type.INT64, value: 1234 });
        this.env.dataset.add({ name: 'sample2', unit: 'lux',
          type: XmlData.Type.ENUM, value: 0, valueName: 'first value',
          enum: [
            { value: 0, name: 'first value' },
            { value: 1, name: 'second value' }
          ]
        });
      })
      .then(() => client.promise())
      .then((value) => {
        expect(value).to.have.lengthOf(2);
        expect(value[0]).to.deep.contain({ index: 0, value: 1234 });
      })
      .then(() => {
        var def = q.defer();
        client.once('value', (value) => {
          expect(value).to.have.lengthOf(2);
          expect(value[0]).to.deep.contain({ index: 0, value: 1235 });
          expect(value[1]).to.deep.contain(
            { index: 1, value: 1, valueName: 'second value' });
          def.resolve();
        });
        client.setParams([
          { index: 0, value: 1235, type: DicXmlParams.DataType.INT64 },
          { index: 1, value: 1, type: DicXmlParams.DataType.ENUM }
        ]);
        return def.promise;
      })
      .finally(() => client.close());
    });

    it('can use nested params', function() {
      var client =
        new DicXmlParams('/pwet', { proxy: env.proxyUrl }, env.dnsUrl);
      return server.run(function() {
        /*:: var serverRequire = require */
        const { XmlData } = serverRequire('@ntof/dim-xml');

        this.env.dataset = this.env.node.addXmlParams('/pwet');
        this.env.dataset.add({ name: 'sample1', unit: 'lux',
          type: XmlData.Type.INT64, value: 1234 });
        this.env.dataset.add({ name: 'sample2',
          value: [
            { name: 'nSample1', unit: 'lux',
              type: XmlData.Type.INT64, value: 1 },
            { name: 'nSample2', unit: 'lux',
              type: XmlData.Type.INT64, value: 2 }
          ] });
        this.env.dataset.add({ name: 'sample3', unit: 'lux',
          type: XmlData.Type.INT64, value: 4321 });

      })
      .then(() => client.promise())
      .then((value) => {
        expect(value).to.have.lengthOf(3);
        expect(value[0]).to.deep.contain({ index: 0, value: 1234 });
        expect(value[1]).to.deep.contain({ index: 1, value: [
          { index: 0, name: 'nSample1', unit: 'lux',
            type: 4, value: 1 },
          { index: 1, name: 'nSample2', unit: 'lux',
            type: 4, value: 2 }
        ] });
        expect(value[2]).to.deep.contain({ index: 2, value: 4321 });
      })
      .then(async () => {
        var def = q.defer();
        client.once('value', (value) => {
          expect(value).to.have.lengthOf(3);
          expect(value[0]).to.deep.contain({ index: 0, value: 1235 });
          expect(value[1].value[0]).to.deep.contain({ index: 0, value: 3 });
          expect(value[1].value[1]).to.deep.contain({ index: 1, value: 4 });
          expect(value[2]).to.deep.contain({ index: 2, value: 5432 });
          def.resolve();
        });
        await client.setParams([ // This is wrong, we should receive only one value event
          { index: 5, value: 'wrong', type: 4 }
        ]).then(() => { throw 'should fail'; }, () => undefined);
        await client.setParams([
          { index: 0, value: 1234, type: 4 },
          { index: 0, value: 1235, type: 4 },
          { index: 1, value: [
            { index: 0, value: 3, type: 4 },
            { index: 1, value: 4, type: 4 }
          ] },
          { index: 2, value: 5432, type: 4 }
        ]);
        return def.promise;
      })
      .finally(() => client.close());
    });
  });
}

describeDicXmlParams('DicXmlParams', null);
describeDicXmlParams('DicXmlParams (noWebSocket)', { noWebSocket: true });
