
const
  chai = require('chai'),
  dirtyChai = require('dirty-chai'),
  { after } = require('mocha'),
  ConnManager = require('../src/ConnManager');

chai.use(dirtyChai);

after(function() {
  ConnManager.release();
});

process.on('unhandledRejection', function(reason) {
  console.log('UnhandledRejection:', reason);
  throw reason;
});
