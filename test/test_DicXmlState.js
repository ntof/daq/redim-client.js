// @ts-check
const
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  q = require('q'),
  _ = require('lodash'),
  utils = require('./utils'),
  server = require('@cern/karma-server-side'),

  { DicXmlState } = require('../src');

describe('DicXmlState', function() {
  var env;
  var token;

  beforeEach(function() {
    this.timeout(15000);
    return utils.server.lock()
    .then((t) => { token = t; })
    .then(() => server.run(function() {
      /*:: var serverRequire = require */
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      const { DnsProxy, DisProxy } = serverRequire('@ntof/redim-dim');
      const { DnsServer } = serverRequire('@cern/dim');
      const utils = serverRequire('./test/server_utils');

      this.env = {};
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer('127.0.0.1', 0);
        return this.env.dns.listen();
      })
      .then(() => {
        this.env.node = new DisXmlNode('127.0.0.1', 0);
        return this.env.node.register('127.0.0.1:' + this.env.dns._conn.port);
      })
      .then(() => {
        DnsProxy.register(this.env.app);
        DisProxy.register(this.env.app);
        var addr = this.env.server.address();
        return {
          node: { port: this.env.node.info.port },
          dns: { port: this.env.dns._conn.port },
          proxyUrl: `http://127.0.0.1:${addr.port}`,
          dnsUrl:
            `http://127.0.0.1:${addr.port}` +
            `/127.0.0.1:${this.env.dns._conn.port}/dns/query`
        };
      });
    }))
    .then((ret) => (env = ret));
  });

  afterEach(function() {
    return utils.server.unlock(token)
    .then(() => server.run(function() {
      if (this.env) {
        this.env.dns.close();
        this.env.server.close();
        this.env.node.close();
        this.env = null;
      }
    }));
  });

  it('can retrieve a value', function() {
    return  q()
    .then(() => server.run(function() {
      this.env.node.addService('/test', 'C', `
        <state value="0" strValue="Idle">
          <value value="-2" strValue="ERROR"/>
          <value value="-1" strValue="NOT READY"/>
          <value value="0" strValue="Idle"/>
          <value value="1" strValue="Initialization"/>
          <error active="false" code="0"/>
          <errors/>
          <warnings/>
        </state>`);
    }))
    .then(() => DicXmlState.get('/test', { proxy: env.proxyUrl }, env.dnsUrl))
    .then((value) => expect(value).to.deep.equal({
      description: [
        { strValue: "ERROR", value: -2 },
        { strValue: "NOT READY", value: -1 },
        { strValue: "Idle", value: 0 },
        { strValue: "Initialization", value: 1 }
      ],
      strValue: "Idle",
      value: 0,
      errors: [],
      warnings: []
    }));
  });

  it('can retrieve an error state', function() {
    var state = new DicXmlState('/test', { proxy: env.proxyUrl }, env.dnsUrl);
    return q()
    .then(() => server.run(function() {
      this.env.node.addService('/test', 'C', `
        <state value="-2" strValue="ERROR">
          <value value="-2" strValue="ERROR"/>
          <value value="-1" strValue="NOT READY"/>
          <value value="0" strValue="Idle"/>
          <value value="1" strValue="Initialization"/>
          <error active="false" code="0"/>
          <errors>
            <error code="404">not found</error>
            <error code="500">server failure</error>
          </errors>
          <warnings>
            <warning code="808">woot !</warning>
          </warnings>
        </state>`);
    }))
    .then(() => state.promise())
    .then((value) => expect(value).to.deep.equal({
      description: [
        { strValue: "ERROR", value: -2 },
        { strValue: "NOT READY", value: -1 },
        { strValue: "Idle", value: 0 },
        { strValue: "Initialization", value: 1 }
      ],
      strValue: "ERROR",
      value: -2,
      errors: [
        { code: 404, message: 'not found' },
        { code: 500, message: 'server failure' }
      ],
      warnings: [
        { code: 808, message: 'woot !' }
      ]
    }))
    .finally(() => state.close());
  });

  it('returns an undefined value when service crashes', function() {
    var state;
    return q()
    .then(() => server.run(function() {
      this.env.node.addService('/test', 'C', `
        <state value="0" strValue="Idle">
          <value value="-2" strValue="ERROR"/>
          <value value="-1" strValue="NOT READY"/>
          <value value="0" strValue="Idle"/>
          <errors/>
          <warnings/>
        </state>`);
    }))
    .then(() => {
      state = new DicXmlState('/test', { proxy: env.proxyUrl }, env.dnsUrl);
    })
    .then(() => state.promise())
    .then(() => {
      return server.run(function() {
        this.env.node.close();
      })
      .then(_.constant(utils.waitEvent(state, 'value', 1)))
      .then((value) => expect(value).to.be.undefined());
    })
    .finally(() => _.invoke(state, 'close'));
  });

  it('can parse xml', function() {
    expect(DicXmlState.parse(`
      <state value="-2" strValue="ERROR">
        <value value="-2" strValue="ERROR"/>
        <error active="false" code="0"/>
        <errors><error code="404">not found</error></errors>
        <errors><error code="500">server failure</error></errors>
        <warnings>
          <warning code="808">woot !</warning>
        </warnings>
      </state>`)).to.deep.equal({
      description: [
        { strValue: "ERROR", value: -2 }
      ],
      strValue: "ERROR",
      value: -2,
      errors: [
        { code: 404, message: 'not found' },
        { code: 500, message: 'server failure' }
      ],
      warnings: [
        { code: 808, message: 'woot !' }
      ]
    });
  });
});
