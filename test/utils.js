// @ts-check
const
  server = require('@cern/karma-server-side'),
  { expect } = require('chai'),
  debug = require('debug')('test'),
  _ = require('lodash'),
  q = require('q');

/*::
declare var serverRequire: (string) => any
*/

function lock() /*: q$Promise<number> */ {
  return q()
  .then(() => server.run(function() {
    const debug = serverRequire('debug')('test');
    if (!this._lock) {
      const { Lock } = serverRequire('./test/server_utils');
      debug('creating lock');
      this._lock = new Lock(1);
      this._lock.token = 1;
    }
    return this._lock.wait()
    .then(() => {
      debug('server locked');
      return ++this._lock.token;
    });
  }));
}

function unlock(token /*: number */) /*: q$Promise<any> */ {
  return q()
  .then(() => server.run(token, function(token) {
    const debug = serverRequire('debug')('test');

    if (!this._lock) {
      debug('server not locked');
      return q.reject();
    }
    else if (token === this._lock.token) {
      debug('server lock released');
      this._lock.post();
    }
    else {
      debug('server lock not owned', token);
      return q.reject();
    }
    return undefined;
  }));
}

function waitEvent(obj /*: any */,
                   evt /*: string */,
                   count /*: ?number */,
                   timeout /*: ?number */) /*: Promise<any> */ {
  var def = q.defer();
  var ret = [];
  var received = 0;

  count = _.defaultTo(count, 1);
  timeout = _.defaultTo(timeout, 1000);

  var cb = function(e) {
    ret.push(e);
    debug('waitEvent: received:%s', evt);
    /* $FlowIgnore */
    if (++received >= count) { def.resolve((count === 1) ? ret[0] : ret); }
  };
  obj.on(evt, cb);
  return def.promise
  .timeout(timeout)
  .finally(function() {
    if (def.promise.isPending()) { def.reject(); }
    obj.removeListener(evt, cb);
  });
}

/**
 * @param  {() => boolean|any} cb
 * @param {string} [msg]
 * @param  {number} [timeout=1000]
 * @return {Q.Promise<any>}
 */
function waitFor(cb, msg = undefined, timeout = 1000) {
  var def = q.defer();
  var resolved = false;
  var err = new Error(msg || 'timeout'); /* create this early to have stacktrace */
  var timer = _.delay(() => {
    resolved = true;
    clearTimeout(nextTimer);
    def.reject(err);
  }, _.defaultTo(timeout, 1000));
  var nextTimer = null;


  function next() {
    if (resolved) { return; }
    try {
      var ret = cb(); /* eslint-disable-line callback-return */
      if (ret) {
        clearTimeout(timer);
        resolved = true;
        def.resolve(ret);
      }
      else {
        nextTimer = _.delay(next, 10);
      }
    }
    catch (e) {
      clearTimeout(timer);
      resolved = true;
      def.reject(_.has(e, 'message') ? e : new Error(e));
    }
  }

  next();
  return def.promise;
}

function throws(fun) {
  try {
    fun();
    return null;
  }
  catch (e) {
    return e.message;
  }
}

/**
 * @template T=any
 * @param  {() => T} test
 * @param  {any} value
 * @param  {string} [msg]
 * @param  {number} [timeout]
 * @return {Q.Promise<T>}
 */
function waitForValue(test, value, msg = undefined, timeout = undefined) {
  /** @type {any} */
  var _val;
  return waitFor(() => {
    _val = test();
    return throws(() => expect(_val).to.deep.equal(value)) === null;
  }, msg, timeout)
  .catch((err) => {
    const msg = throws(expect(_val).to.deep.equal(value)) ||
      ("Invalid result value: " + _.toString(_val) + " != " + value);
    if (err instanceof Error) {
      err.message = msg;
      throw err;
    }
    throw new Error(msg);
  });
}

module.exports = {
  server: { lock, unlock }, waitEvent, waitForValue, waitFor
};
