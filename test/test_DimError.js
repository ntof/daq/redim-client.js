// @ts-check
const
  { describe, it } = require('mocha'),
  { expect } = require('chai'),
  _ = require('lodash'),

  { DimError } = require('../src'),
  { wrapError } = require('../src/utils');


describe('DimError', function() {

  it('can create errors', function() {
    _.forEach([
      'NetworkError', 'Timeout', 'NotFound', 'InvalidParam', 'NotSupported',
      'NetworkError'
    ], function(type) {
      var err = new DimError[type]('sample message');
      expect(err).to.have.property('name', type);
      expect(_.toString(err).startsWith(type + ': sample message'))
      .to.be.true();
    });
  });

  it('can wrap errors', function() {
    const err = wrapError(DimError.NetworkError, new Error('random error'));
    expect(err).to.have.property('message', 'random error');
    expect(err).to.have.property('name', 'NetworkError');
  });
});
