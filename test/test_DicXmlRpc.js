// @ts-check
const
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  utils = require('./utils'),
  server = require('@cern/karma-server-side'),

  { DicCmdUtils, DicXmlRpc, xml } = require('../src');

describe('DicXmlRpc', function() {
  var env;
  var token;

  beforeEach(function() {
    this.timeout(15000);
    return utils.server.lock()
    .then((t) => { token = t; })
    .then(() => server.run(function() {
      /*:: var serverRequire = require */
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      const { DnsProxy, DisProxy } = serverRequire('@ntof/redim-dim');
      const { DnsServer } = serverRequire('@cern/dim');
      const utils = serverRequire('./test/server_utils');

      this.env = {};
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer('127.0.0.1', 0);
        return this.env.dns.listen();
      })
      .then(() => {
        this.env.node = new DisXmlNode('127.0.0.1', 0);
        // this.env.node.
        return this.env.node.register('127.0.0.1:' + this.env.dns._conn.port);
      })
      .then(() => {
        DnsProxy.register(this.env.app);
        DisProxy.register(this.env.app);
        var addr = this.env.server.address();
        return {
          node: { port: this.env.node.info.port },
          dns: { port: this.env.dns._conn.port },
          proxyUrl: `http://127.0.0.1:${addr.port}`,
          dnsUrl: `http://127.0.0.1:${addr.port}` +
          `/127.0.0.1:${this.env.dns._conn.port}/dns/query`
        };
      });
    }))
    .then((ret) => (env = ret));
  });

  afterEach(function() {
    return utils.server.unlock(token)
    .then(() => server.run(function() {
      if (this.env) {
        this.env.dns.close();
        this.env.server.close();
        this.env.node.close();
        this.env = null;
      }
    }));
  });

  it('can make a call', function() {
    return server.run(function() {
      this.env.node.addXmlRpc('/pwet', function() {});
    })
    .then(() => DicXmlRpc.invoke('/pwet', { test: 42 },
      { key: 666, proxy: env.proxyUrl }, env.dnsUrl))
    .then((ret) => {
      return expect(ret).to.deep.contain(
        { error: 0, key: 666, status: 2 });
    });
  });

  it('can send and retrieve information', function() {
    var cmd = new DicXmlRpc('/pwet', { proxy: env.proxyUrl }, env.dnsUrl);
    return server.run(function() {
      /*:: var serverRequire = require */
      const { expect } = serverRequire('chai');
      const { xml } = serverRequire('./src');
      serverRequire('./test/server_utils'); /* dirty-chai init */

      this.env.node.addXmlRpc('/pwet', (req, ret) => {
        expect(req.xml).to.be.ok();
        var params = xml.toJs(req.xml);
        expect(params).to.deep.contains({ pwet: '42' });
        xml.fromJs(ret, params);
      });
    })
    .then(() => cmd.invoke({ pwet: 42 }, 1234))
    .then((rep) => {
      var params = xml.toJs(rep.xml);
      expect(params).to.deep.contains({ pwet: '42' });
    })
    .finally(() => cmd.close());
  });

  it('can handle errors', function() {
    return server.run(function() {
      /*:: var serverRequire = require */
      const { DicCmdUtils } = serverRequire('./src');

      this.env.node.addXmlRpc('/pwet', () => {
        throw DicCmdUtils.createError(12, 'sample');
      });
    })
    .then(() => DicXmlRpc.invoke('/pwet', { test: 42 }, { proxy: env.proxyUrl },
      env.dnsUrl))
    .then(
      () => expect.fail('should fail'),
      (ret) => expect(ret).to.contain(
        { error: 12, status: DicCmdUtils.Status.ERROR, message: 'sample' }));
  });
});
