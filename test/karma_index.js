
const
  { before, after } = require('mocha'),
  chai = require('chai'),
  dirtyChai = require('dirty-chai'),
  server = require('@cern/karma-server-side'),
  ConnManager = require('../src/ConnManager');

function isBrowser() {
  return (typeof process === 'undefined') ||
    (process.type === 'renderer') ||
    (process.browser === true);
}

// Uncomment this if you want to see redim client logs
// localStorage['debug'] = 'redim*';

if (isBrowser()) {
  chai.use(dirtyChai);

  /* must be done early */
  require('debug').useColors = () => false; /* eslint-disable-line global-require */

  before(function() {
    return server.run(function() {
      // add a guard to not add one event listener per test
      if (!this.karmaInit) {
        // $FlowIgnore
        this.karmaInit = true;
        /* do not accept unhandledRejection */
        process.on('unhandledRejection', function(reason) {
          console.log('UnhandledRejection:', reason);
          throw reason;
        });
      }
      return process.env['DEBUG'];
    })
    .then((dbg) => {
      localStorage['debug'] = dbg;
    });
  });

  after(function() {
    ConnManager.release();
  });
}
