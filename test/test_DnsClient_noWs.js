// @ts-check
const
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  server = require('@cern/karma-server-side'),
  utils = require('./utils'),
  { DnsClient } = require('../src');

/*::
import { DnsServer, DisNode } from '@cern/dim'
declare var serverRequire: (string) => any
*/

describe('DnsClient (noWebSocket)', function() {
  var env;
  var token;

  beforeEach(function() {
    this.timeout(15000); /* wait for other browsers to finish */
    return utils.server.lock()
    .then((t) => { token = t; })
    .then(() => server.run(function() {
      const { DnsServer /*: DnsServer */ } = serverRequire('@cern/dim');
      const { DnsProxy } = serverRequire('@ntof/redim-dim');
      const utils = serverRequire('./test/server_utils');

      this.env = {};
      return utils.createApp(this.env, { noWebSocket: true })
      .then(() => {
        this.env.dns = new DnsServer('127.0.0.1', 0);
        return this.env.dns.listen();
      })
      .then(() => {
        DnsProxy.register(this.env.app);

        var addr = this.env.server.address();
        var dnsUrl = '127.0.0.1:' + this.env.dns._conn.port;
        return `http://127.0.0.1:${addr.port}/${dnsUrl}/dns/query`;
      });
    }))
    .then((address) => { env = { address }; });
  });

  afterEach(function() {
    return utils.server.unlock(token)
    .then(() => server.run(function() {
      if (this.env) {
        this.env.dns.close();
        this.env.server.close();
        this.env = null;
      }
    }));
  });

  it('can query a dns', function() {
    var dns = new DnsClient(env.address);
    return dns.query('DIS_DNS/SERVICE_INFO/RpcIn')
    .then((info) => expect(info).to.deep.contain({
      node: '127.0.0.1', definition: 'C|CMD'
    }))
    .finally(() => dns.close());
  });
});
