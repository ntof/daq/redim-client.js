// @ts-check
const
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  q = require('q'),
  utils = require('./utils'),
  server = require('@cern/karma-server-side'),

  { DicXmlDataSet } = require('../src');

function describeDicXmlDataSet(name) {
  describe(name, function() {
    var env;
    var token;

    beforeEach(function() {
      this.timeout(15000);
      return utils.server.lock()
      .then((t) => {
        token = t;
      })
      .then(() => server.run(function() {
        /*:: var serverRequire = require */
        const { DisXmlNode } = serverRequire('@ntof/dim-xml');
        const { DnsProxy, DisProxy } = serverRequire('@ntof/redim-dim');
        const { DnsServer } = serverRequire('@cern/dim');
        const utils = serverRequire('./test/server_utils');

        this.env = {};
        return utils.createApp(this.env)
        .then(() => {
          this.env.dns = new DnsServer('127.0.0.1', 0);
          return this.env.dns.listen();
        })
        .then(() => {
          this.env.node = new DisXmlNode('127.0.0.1', 0);
          return this.env.node.register('127.0.0.1:' + this.env.dns._conn.port);
        })
        .then(() => {
          DnsProxy.register(this.env.app);
          DisProxy.register(this.env.app);
          var addr = this.env.server.address();
          return {
            node: { port: this.env.node.info.port },
            dns: { port: this.env.dns._conn.port },
            proxyUrl: `http://127.0.0.1:${addr.port}`,
            dnsUrl: `http://127.0.0.1:${addr.port}` +
              `/127.0.0.1:${this.env.dns._conn.port}/dns/query`
          };
        });
      }))
      .then((ret) => (env = ret));
    });

    afterEach(function() {
      return utils.server.unlock(token)
      .then(() => server.run(function() {
        if (this.env) {
          this.env.dns.close();
          this.env.server.close();
          this.env.node.close();
          this.env = null;
        }
      }));
    });

    it('can retrieve a dataset and nested datasets', function() {
      var client = new DicXmlDataSet('/pwet', { proxy: env.proxyUrl },
        env.dnsUrl);
      return server.run(function() {
        /*:: var serverRequire = require */
        const { XmlData } = serverRequire('@ntof/dim-xml');

        this.env.dataset = this.env.node.addXmlDataSet('/pwet');
        this.env.dataset.add({
          name: 'sample1', unit: 'lux',
          type: XmlData.Type.INT64, value: 1234
        });
        this.env.dataset.add({
          name: 'sample2', unit: 'lux',
          type: XmlData.Type.ENUM, value: 0, valueName: 'first value',
          enum: [
            { value: 0, name: 'first value' },
            { value: 1, name: 'second value' }
          ]
        });
        this.env.dataset.add({
          name: 'sample3', value: [
            {
              name: 'nestedSample1', unit: 'lux',
              type: XmlData.Type.INT64, value: 4321
            },
            {
              name: 'nestedSample2', unit: 'lux',
              type: XmlData.Type.STRING, value: "4321"
            }
          ]
        });
      })
      .then(() => client.promise())
      .then((value) => expect(value).to.deep.equal([
        {
          index: 0, name: 'sample1', unit: 'lux',
          type: DicXmlDataSet.DataType.INT64, value: 1234
        },
        {
          index: 1,
          name: 'sample2',
          unit: 'lux',
          type: DicXmlDataSet.DataType.ENUM,
          value: 0,
          valueName: 'first value',
          enum: [
            { value: 0, name: 'first value' },
            { value: 1, name: 'second value' }
          ]
        },
        {
          index: 2, name: 'sample3',
          value: [
            {
              index: 0, name: 'nestedSample1', unit: 'lux',
              type: DicXmlDataSet.DataType.INT64, value: 4321
            },
            {
              index: 1, name: 'nestedSample2', unit: 'lux',
              type: DicXmlDataSet.DataType.STRING, value: "4321"
            }
          ]
        }
      ]))
      .then(() => {
        var def = q.defer();
        client.once('value', (value) => {
          expect(value).to.be.an('array');
          expect(value[0]).to.deep.contain({ index: 0, value: 42 });
          expect(value[1]).to.deep.contain(
            { index: 1, value: 1, valueName: 'second value' });
          expect(value[2]).to.deep.contain(
            { index: 2, name: 'sample3', value: [
              {
                index: 0, name: 'nestedSample1', unit: 'lux',
                type: DicXmlDataSet.DataType.INT64, value: 5432
              },
              {
                index: 1, name: 'nestedSample2', unit: 'lux',
                type: DicXmlDataSet.DataType.INT64, value: 7654
              }
            ] });
          def.resolve();
        });
        return server.run(function() {
          this.env.dataset.update({ index: 0, value: 42 }); /* only one event should be sent in deferred */
          this.env.dataset.update(1, 1);
          this.env.dataset.update({ index: 2, value: [
            { index: 0, value: 5432 },
            { index: 1, value: 7654, type: 4 }
          ] }, true);
        })
        .then(() => def.promise);
      })
      .finally(() => client.close());
    });
  });
}


describeDicXmlDataSet('DicXmlDataSet');
describeDicXmlDataSet('DicXmlDataSet (noWebSocket)');
