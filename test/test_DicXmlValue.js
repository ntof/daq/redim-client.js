// @ts-check
const
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  q = require('q'),
  utils = require('./utils'),
  server = require('@cern/karma-server-side'),
  _ = require('lodash'),

  { DicXmlValue } = require('../src');

function describeDicXmlValue(name) {
  describe(name, function() {
    var env;
    var token;

    beforeEach(function() {
      this.timeout(15000);
      return utils.server.lock()
      .then((t) => { token = t; })
      .then(() => server.run(function() {
      /*:: var serverRequire = require */
        const { DisXmlNode } = serverRequire('@ntof/dim-xml');
        const { DnsProxy, DisProxy } = serverRequire('@ntof/redim-dim');
        const { DnsServer } = serverRequire('@cern/dim');
        const utils = serverRequire('./test/server_utils');

        this.env = {};
        return utils.createApp(this.env)
        .then(() => {
          this.env.dns = new DnsServer('127.0.0.1', 0);
          return this.env.dns.listen();
        })
        .then(() => {
          this.env.node = new DisXmlNode('127.0.0.1', 0);
          return this.env.node.register('127.0.0.1:' + this.env.dns._conn.port);
        })
        .then(() => {
          DnsProxy.register(this.env.app);
          DisProxy.register(this.env.app);
          var addr = this.env.server.address();
          return {
            node: { port: this.env.node.info.port },
            dns: { port: this.env.dns._conn.port },
            proxyUrl: `http://127.0.0.1:${addr.port}`,
            dnsUrl:
              `http://127.0.0.1:${addr.port}` +
              `/127.0.0.1:${this.env.dns._conn.port}/dns/query`
          };
        });
      }))
      .then((ret) => (env = ret));
    });

    afterEach(function() {
      return utils.server.unlock(token)
      .then(() => server.run(function() {
        if (this.env) {
          this.env.dns.close();
          this.env.server.close();
          this.env.node.close();
          this.env = null;
        }
      }));
    });

    it('can retrieve a value', function() {
      return  q()
      .then(() => server.run(function() {
        this.env.node.addService('/test', 'C', '<test value="42" />');
      }))
      .then(() => DicXmlValue.get('/test', { proxy: env.proxyUrl }, env.dnsUrl))
      .then((value) => {
        expect(_.invoke(value, 'getAttribute', 'value')).to.equal('42');
      });
    });

    it('returns an undefined value when service crashes', function() {
      var xmlValue;
      return  q()
      .then(() => server.run(function() {
        this.env.node.addService('/test', 'C', '<test value="42" />');
      }))
      .then(() => {
        xmlValue =
          new DicXmlValue('/test', { proxy: env.proxyUrl }, env.dnsUrl);
      })
      .then(() => xmlValue.promise())
      .then(() => {
        return server.run(function() {
          this.env.node.close();
        })
        .then(_.constant(utils.waitEvent(xmlValue, 'value', 1)))
        .then((value) => expect(_.invoke(value, 'getAttribute', 'value'))
        .to.be.undefined());
      })
      .finally(() => _.invoke(xmlValue, 'close'));
    });
  });
}

describeDicXmlValue('DicXmlValue');
describeDicXmlValue('DicXmlValue (noWebSocket)');
