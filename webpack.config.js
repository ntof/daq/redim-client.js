var path = require('path');
var webpack = require('webpack');
const _ = require('lodash');

module.exports = {
  mode: 'development',
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'index.js'
  },
  module: {
    rules: [
      { test: /\.js$/, loader: 'babel-loader' }
    ]
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true,
    overlay: true
  },
  performance: {
    hints: "warning"
  },
  devtool: 'inline-cheap-module-source-map',
  plugins: [
    new webpack.ProvidePlugin({
      Buffer: [ 'buffer', 'Buffer' ]
    })
  ]
};

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = 'source-map';
  module.exports.mode = 'production';
  _.set(module.exports, 'optimization.nodeEnv', 'production');
  _.set(module.exports, 'optimization.moduleIds', 'named');
}
